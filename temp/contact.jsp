<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
        <section class="titlebar">
            <h1 class="page-title"><span>Contact </span>us</h1>
            <div id="particles-js"></div>
        </section>
        
        <hr class="col-md-6 bottom_60">

        <div class="cont">
            <section class="contact col-md-8 offset-md-2 top_90">
                <div class="contact-info text-center">
                    <p>YongSan-Gu, Seoul, South Korea</p> 
                    <a href="dev.stillknow@gmail.com">dev.stillknow@gmail.com</a> 
                    <p>010 9882 6987</p>
                </div>
                <div class="contact-form top_90">
                    <form id="emailForm" class="row" action="/sendEmail.jsp" method="post">
                        <div class="col-md-6">
                            <input id="name" name="name" class="inp" type="text" placeholder="Name">
                        </div>
                        <div class="col-md-6">
                            <input id="email" name="email" class="inp" type="text" placeholder="Email">
                        </div>
                        <div class="col-md-12">
                            <textarea id="msg" name="msg" placeholder="Your Message" rows="6" class="col-md-12 form-message"></textarea>
                        </div>
                        <div class="col-md-12 text-center">
                            <input id="submit" type="submit" value="Submit" class="site-btn2">
                        </div>
                    </form>                    
                </div>
            </section>
        </div> <!-- cont end -->
        <script src="/html/js/jquery.form.js"></script>
<script type="text/javascript">
$('#submit').click(function(e){
	e.preventDefault();
	var form = $('#emailForm').ajaxSubmit({
        url: '/sendEmail.jsp',
        dataType: 'json',
        timeout: 10000,
        beforeSubmit: showRequest,
        success: function(result, textStatus){ 
            if(result.code=="00"){ 
                alert("성공"); 
            }else{ 
                alert("실패"); 
            } 
        }, 
        error:function(XMLHttpRequest, textStatus, errorThrown){ 
			console.log(XMLHttpRequest);
			console.log(textStatus);
            alert("실패"); 
        } 
    });
	function processJson(result, textStatus) {
	    //debugger;
	    alert("it worked" + result);
	    console.log("respose: " + result);
	}
	function showRequest(formData, jqForm, options) {
	    //debugger;
	    var queryString = $.param(formData);
	    console.log('About to submit: \n' + queryString + '\n');
	    return true;
	}
	/*
    .done(function (result) {
        t.updateSinglePage(result);
        alert("전송 되었습니다.");
        //$("#emailForm").reset();
    })
    .fail(function () {
        t.updateSinglePage('AJAX Error! Please refresh the page!');
    });
	*/
});
</script>
