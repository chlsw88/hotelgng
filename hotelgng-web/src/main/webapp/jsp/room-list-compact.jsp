<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
        <div class="head_panel">
          <div style="background-image: url('./assets/images/page-head-2.jpg')" class="full_width_photo transparent_film">
            <div class="container">
              <div class="caption">
                <h1>Rooms List Compact</h1><span>View details for every room we have, in a different view</span>
              </div>
            </div>
          </div>
        </div>
        <section class="light_section">
          <div class="container">
            <div class="row">
              <div class="col-md-12 text-left">
                <ul class="breadcrumb">
                  <li><a href="#!">HOME</a></li>
                  <li><span>ROOMS LIST</span></li>
                </ul>
              </div>
              <div class="col-md-6 small_screen_margin_top_half">
                <div class="col-md-12">
                  <div class="row"><a href="room-single.html"><img src="assets/images/room-4.jpg" alt="Room Image"></a></div>
                </div>
                <div class="col-md-12 room_bg_light compact_width_room">
                  <div class="text_block">
                    <h2> <a href="room-single.html">Single Room </a><small><a href="room-single.html">- 5 Reviews</a></small></h2>
                    <div><i class="icon icon-TV"></i><i class="icon icon-Signal"></i><i class="icon icon-Espresso"></i></div>
                    <p class="text-justify">Over, dominion own it above gathering their, don't won't waters bring male bearing form may rule doesn't him fish appear spirit let earth may life you'll to great Tree moveth midst a there so Blessed saw fly don't multiply, dry.le doesn't him fish appear spirit let earth may life you'll to great.</p><a href="room-single.html" class="btn btn-primary">READ MORE</a>
                  </div>
                </div>
              </div>
              <div class="col-md-6 small_screen_margin_top_half">
                <div class="col-md-12">
                  <div class="row"><a href="room-single.html"><img src="assets/images/room-1.jpg" alt="Room Image"></a></div>
                </div>
                <div class="col-md-12 room_bg_light compact_width_room">
                  <div class="text_block">
                    <h2> <a href="room-single.html">Double Room </a><small><a href="room-single.html">- 5 Reviews</a></small></h2>
                    <div><i class="icon icon-Signal"></i><i class="icon icon-TV"></i><i class="icon icon-Espresso"></i></div>
                    <p class="text-justify">Over, dominion own it above gathering their, don't won't waters bring male bearing form may rule doesn't him fish appear spirit let earth may life you'll to great Tree moveth midst a there so Blessed saw fly don't multiply, dry.le doesn't him fish appear spirit let earth may life you'll to great.</p><a href="room-single.html" class="btn btn-primary">READ MORE</a>
                  </div>
                </div>
              </div>
              <div class="col-md-6 margin_top_half">
                <div class="col-md-12">
                  <div class="row"><a href="room-single.html"><img src="assets/images/room-3.jpg" alt="Room Image"></a></div>
                </div>
                <div class="col-md-12 room_bg_light compact_width_room">
                  <div class="text_block">
                    <h2> <a href="room-single.html">Suite with view </a><small><a href="room-single.html">- 5 Reviews</a></small></h2>
                    <div><i class="icon icon-TV"></i><i class="icon icon-Espresso"></i><i class="icon icon-Signal"></i></div>
                    <p class="text-justify">Over, dominion own it above gathering their, don't won't waters bring male bearing form may rule doesn't him fish appear spirit let earth may life you'll to great Tree moveth midst a there so Blessed saw fly don't multiply, dry.le doesn't him fish appear spirit let earth may life you'll to great.</p><a href="room-single.html" class="btn btn-primary">READ MORE</a>
                  </div>
                </div>
              </div>
              <div class="col-md-6 margin_top_half">
                <div class="col-md-12">
                  <div class="row"><a href="room-single.html"><img src="assets/images/room-2.jpg" alt="Room Image"></a></div>
                </div>
                <div class="col-md-12 room_bg_light compact_width_room">
                  <div class="text_block">
                    <h2> <a href="room-single.html">Deluxe king suite</a><small><a href="room-single.html">- 5 Reviews</a></small></h2>
                    <div><i class="icon icon-Signal"></i><i class="icon icon-Espresso"></i><i class="icon icon-TV"></i></div>
                    <p class="text-justify">Over, dominion own it above gathering their, don't won't waters bring male bearing form may rule doesn't him fish appear spirit let earth may life you'll to great Tree moveth midst a there so Blessed saw fly don't multiply, dry.le doesn't him fish appear spirit let earth may life you'll to great.</p><a href="room-single.html" class="btn btn-primary">READ MORE</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>