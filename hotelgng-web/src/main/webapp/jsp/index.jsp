<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<!-- script start -->
<jsp:include page="/jsp/include/GngScrpit.jsp"></jsp:include> 
  <body style="">
    <div id="page_wrapper">
	<!-- header start -->
	<jsp:include page="/jsp/include/GngMainHead.jsp"></jsp:include>
	
	<!-- ==================== SCRIPTS | CONFIG ====================-->
    <script type="text/javascript" src="./assets/js/config.js" data-module="main-configuration"></script>
    <!-- ==================== SCRIPTS | GLOBAL ====================-->
    <script type="text/javascript" src="./assets/js/libs/jquery-2.1.4.js"></script>
    <script type="text/javascript" src="./assets/js/libs/bootstrap.js"></script>
    <script type="text/javascript" src="./assets/js/libs/wow.js" data-module="wow-animation-lib"></script>
    <script type="text/javascript" src="./assets/js/libs/conformity/dist/conformity.js" data-module="equal-column-height"></script>
    <!-- END====================== SCRIPTS ========================-->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAhCFO56k_xL212g8j2LK88wK0I_CRwzDE&amp;sensor=false" data-module="google-maps"></script>
    <!-- =================== SCRIPTS | SECTIONS ===================-->
    <script type="text/javascript" src="./assets/js/libs/owl.carousel.2.0.0-beta.2.4/owl.carousel.js" data-module="owlslider"></script>
    <!-- END================ SCRIPTS | SECTIONS ===================-->
    <script type="text/javascript" src="./assets/js/libs/moment.js"></script>
    <script type="text/javascript" src="./assets/js/libs/bootstrap-datetimepicker.js"></script>
      <div class="main" id="contentMain">
      <c:if test="${not empty cookie.page.value}">
      	<jsp:include page="/jsp/${cookie.page.value}"></jsp:include>
      </c:if>
      <c:if test="${empty cookie.page.value}">
      	<jsp:include page="/jsp/main.jsp"></jsp:include>
      </c:if>
      </div>
      <!-- ============================ FOOTER ============================-->
   	 <jsp:include page="/jsp/include/GngFooter.jsp"></jsp:include>
	 <jsp:include page="/jsp/include/GngSubHeader.jsp"></jsp:include>
    </div>
  </body>
</html>