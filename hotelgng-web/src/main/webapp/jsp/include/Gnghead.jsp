<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
      <div class="header click_menu transparent">
        <!-- =========================== HEADER ==========================-->
        <div class="mainbar">     
          <div class="container-fluid">
            <div class="logo" id="logoDiv"><a href="main.jsp" class="brand"><img src="./assets/images/logo_white_grad3.png" alt="logo"></a></div>
            <div class="nav_and_tools nav_centered uppercase">    
              <nav class="primary_nav">
                <ul class="nav" id="navMenuUl" >
                  <li><a href="main.jsp">Home</a></li>
                  <li><a href="room-list.jsp">Rooms</a></li>
                  <li><a href="location.jsp">Location</a></li>
                  <li><a href="index-activities.jsp">Activities</a></li>
                  <li><a href="index-restaurant.jsp">Restaurant</a></li>
                  <li><a href="gallery.jsp">Gallery</a></li>
                  <li><a href="blog.jsp">Blog</a></li>
                  <li><a href="contact.jsp">Contact</a></li>
                </ul>
              </nav>
            </div>
          </div><a class="menu-toggler"><span class="title"></span><span class="lines"></span></a>
        </div>
        <!-- END======================== HEADER ==========================-->
      </div>