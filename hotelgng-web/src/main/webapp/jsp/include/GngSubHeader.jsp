<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
      <div class="secondary_nav_widgetized_area"> 
        <nav>
         <!--  <ul>
            <li><a href="index-gym.jsp">Gym</a></li>
            <li><a href="index-spa.jsp">Spa</a></li>
            <li><a href="index-package-offer.jsp">Package</a></li>
            <li><a href="room-single.jsp">Room Single </a></li>
            <li><a href="blog-boxed.jsp">Blog Masonry</a></li>
            <li><a href="room-list-compact.jsp">Room List Grid</a></li>
            <li><a href="single.jsp">Post Single </a></li>
            <li><a href="booking.jsp">Booking</a></li>
            <li><a href="404.jsp">404</a></li> 
          </ul> -->
        </nav>
        <aside class="widget widget_text">
          <h4>ABOUT CITYC HOTEL</h4>
          <div class="textwidget text-justify">   Either you look for a luxurious experience or a remarkable on the go experience, Xenia Resort is the place to visit and spend the time at. We offer quality services.</div>
          <div class="text_block">
            <div><i class="icon icon-Phone"></i>	<spring:message code="hotelgng.tel.number" />	</div>
            <div><i class="icon icon-Flag"></i>		<spring:message code="hotelgng.address" />		</div>
            <div><i class="icon icon-Keyboard"></i>	<spring:message code="hotelgng.admin.email" />	</div>
          </div>
        </aside>
        <div class="col-md-12">
          <div class="secondary_widgetized_area_copyright small_screen_text_center">
            <p>COPYRIGHT Â© 2015 ALL RIGHTS RESERVED</p>
          </div>
        </div>
      </div>