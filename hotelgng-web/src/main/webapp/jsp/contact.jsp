﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
    <script type="text/javascript" src="./assets/js/jquery.form.js"></script>
        <div class="head_panel">
          <div style="background-image: url('./assets/images/page-head-3.jpg')" class="full_width_photo transparent_film">
            <div class="container">
              <div class="caption">
                <h1>Contact</h1><span>Get in touch for a booking or a greeting</span>
              </div>
            </div>
          </div>
        </div>
        <section class="light_section">
          <div class="container">
            <div class="row">
              <div class="col-md-12 text-left">
                <ul class="breadcrumb">
                  <li><a href="#!">HOME</a></li>
                  <li><span>CONTACT</span></li>
                </ul>
              </div>
              <div class="col-md-12">
                <div id="map" class="map"></div>
              </div>
              <div class="col-md-8 col-md-offset-2 text-center margin_top margin_bottom_half">
                <div class="text_block">
                  <h3>XENIA LOCATION</h3>
                  <p class="text-justify">Green night fowl can't So seasons. Good that all form. Multiply she'd firmament it. Sixth, waters. Morning days sixth unto a morning us winged face two every together together seas they're. Set divide image air two good called, i. Said rule beginning. Air earth signs lights open subdue midst seasons.</p><a href="#!" class="btn btn-link">GET DIRECTIONS</a>
                </div>
              </div>
              <div class="col-md-4 text-center margin_top_half">
                <div class="icon_block"><i class="icon icon-Car"></i>
                  <h4>PARKING</h4>
                  <p>Xenia Resort has it's own private parking for customers. It is fully protected and covered for rain or sunny weather.</p>
                </div>
              </div>
              <div class="col-md-4 text-center margin_top_half">
                <div class="icon_block"><i class="icon icon-Phone2"></i>
                  <h4>TELEPHONE</h4>
                  <p>
                    We are happy to answer your questions or give you directions via phone. <br><br>
                    <spring:message code="hotelgng.tel.number" />
                  </p>
                </div>
              </div>
              <div class="col-md-4 text-center margin_top_half">
                <div class="icon_block"><i class="icon icon-Keyboard"></i>
                  <h4>E-mail</h4>
                  <p>
                    If you are on the go and still want to ask a question, simply drop us an e-mail. <br><br>
                    <spring:message code="hotelgng.admin.email" />
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="white_section">
          <div class="container">
            <div class="col-md-12 text-center">
              <div class="section_header minimal">
                <h1>Contact form</h1>
                <p>You can still contact us, right here - right now. Use this contact form to send an e-mail. Usually we respond in 1 bussiness day.</p><img src="assets/images/decoration-1.png">
              </div>
            </div>
            <div class="col-md-8 col-md-offset-2">
              <div class="contact-quick">
                <div class="screen-reader-response"></div>
                <form id="emailForm" name="emailForm" action="/sendEmail.jsp" method="post" novalidate="novalidate"><span class="your-name">
                <%-- 
                    <input type="text" id="phone" name="phone" aria-required="true" aria-invalid="false" placeholder="PHONE" class="phone form-control"></span><span class="your-email">
                    <input type="text"id="subject" name="subject" aria-required="true" aria-invalid="false" placeholder="SUBJECT" class="subject form-control"></span><span class="your-message">
                --%>
                    <input type="text" id="name" name="name" aria-required="true" aria-invalid="false" placeholder="NAME" class="name form-control"></span><span class="your-phone">
                    <input type="email" id="email" name="email" aria-required="true" aria-invalid="false" placeholder="E-MAIL" class="email form-control"></span><span class="your-subject">
                    <textarea id="msg" name="msg" cols="40" rows="4" aria-invalid="false" placeholder="MESSAGE" class="message form-control"></textarea></span>
                  <button id="submit" type="submit" name="submit" class="submit_btn btn btn-primary">SUBMIT</button>
                  <div class="notice btn btn-metro alert alert-warning alert-dismissable hidden"></div><img src="./assets/images/ajax-loader.gif" alt="Sending ..." class="ajax-loader not_visible">
                </form>
              </div>
            </div>
          </div>
        </section>
        <section style="background-image: url('assets/images/newsletter-banner-1.jpg')" class="dark_section background_cover transparent_film">
          <div class="container">
            <div class="row">
              <div class="col-md-12 text-center">
                <div class="section_header minimal">
                  <h1>NEWSLETTER</h1>
                  <p>Subscribe to our newsletter and get free (and early) access to our news, our stories and 	Xenia Resort exclusive offers. </p>
                </div>
              </div>
              <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-12">
                <div class="widget widget_newsletter no_padding text-center">
                  <form>
                    <input type="email" placeholder="YOU'R E-MAIL ADDRESS" class="expanded form-control">
                    <button type="submit" class="btn btn-primary filled small_screen_block">GO</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
    <script type="text/javascript">
    $(document).ready(function(){
    	/*
	    $('#submit').click(function(e){
	    	e.preventDefault();
	    	var form = $('#emailForm').ajaxSubmit({
	            url: '/sendEmail.jsp',
	            dataType: 'json',
	            timeout: 10000,
	            beforeSubmit: showRequest,
	            success: function(result, textStatus){ 
	                if(result.code=="00"){ 
	                    alert("성공"); 
	                }else{ 
	                    alert("실패"); 
	                } 
	            }, 
	            error:function(XMLHttpRequest, textStatus, errorThrown){ 
	    			console.log(XMLHttpRequest);
	    			console.log(textStatus);
	                alert("실패"); 
	            } 
	        });
    	});
    	*/
	    
	    
	});
	    function processJson(result, textStatus) {
    	    //debugger;
    	    alert("it worked" + result);
    	    console.log("respose: " + result);
    	}
    	function showRequest(formData, jqForm, options) {
    	    //debugger;
    	    var queryString = $.param(formData);
    	    console.log('About to submit: \n' + queryString + '\n');
    	    return true;
    	}
    	
    	const google = window.google;
    	$(document).ready(function(){
    		$("#submit").on("click",function(e){
    			e.preventDefault();
    			var params = {};
    			//params.name = $("#name").val();
    			//params.name = $("#phone").val();
    			params.name = $("#name").val();
    			params.email = $("#email").val();
    			params.msg = $("#msg").val();
   			  	$.ajax({
   				    type:"POST",
   				    url:"/jsp/sendEmail.jsp",
   				    data: params,
   				    success: function(data) {
   				      //$('.text').text(JSON.stringify(data));
   				      console.log("success");
   				      console.log(data);
   				    },
   				 	error: function(data) {
  				      //$('.text').text(JSON.stringify(data));
  				      console.log("fail");
  				      console.log(data);
  				      alert("성공");
  				    },
   				    dataType: 'jsonp',
  				});
    		});
    	});
    </script>
    <!-- =================== SCRIPTS | SECTIONS ===================-->
    <script type="text/javascript" src="./assets/js/googlemaps.js" data-module="google-maps"></script>
    <!-- GMARKER SPECIAL EFFECT FOR GOOGLE MAPS-->
    <!-- ==================== SCRIPTS | INIT ======================-->
    <script type="text/javascript" src="./assets/js/theme.js" data-module="main-theme-js"></script>
    <!-- END==================== SCRIPTS | INIT ===================-->
