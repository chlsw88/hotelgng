$(document).ready(function(){
	$("#navMenuUl li, #logoDiv, #contentMain").on("click","a",function(e){
		e.preventDefault();
		createView($(this).attr("href"),true);
	});
});

//general functions
function getUrl(u) {
     return "/jsp/" + u;
}
function loadURL(u)    {
	$("#contentMain").load(getUrl(u));
}

function createView(stateObject, pushHistory) {
	setCookie("page",stateObject,180);
	loadURL(stateObject);
	currentPage = stateObject;
 
	// Save state on history stack
	// - First argument is any object that will let you restore state
	// - Second argument is a title (not the page title, and not currently used)
	// - Third argument is the URL - this will appear in the browser address bar
	//if (pushHistory) history.pushState(stateObject, stateObject.title, '?page='+stateObject.contentId);
	if (pushHistory) history.pushState(stateObject, stateObject, '?page='+stateObject);
}
window.onpopstate = function(event) {
	// We use false as the second argument below 
	// - state will already be on the stack when going Back/Forwards
	createView(event.state, false);
};

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}