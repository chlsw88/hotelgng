﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.io.*,java.util.*,javax.mail.*"%>
<%@ page import="javax.mail.internet.*,javax.activation.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<%
   String result;
   String to = "dev.stillknow@gmail.com";
   String name = request.getParameter("name");
   String email = request.getParameter("email");
   String subject = request.getParameter("subject");
   String phone = request.getParameter("phone");
   String msg = request.getParameter("message");
   String host = "localhost";
   Properties properties = System.getProperties();
   properties.setProperty("mail.smtp.host", host);
   Session mailSession = Session.getDefaultInstance(properties);
   try{
      MimeMessage message = new MimeMessage(mailSession);
      message.setFrom(new InternetAddress(email));
      message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));
      message.setSubject("This is the Subject Line!"+subject);
      BodyPart messageBodyPart = new MimeBodyPart();
      messageBodyPart.setText("This is message body"+msg);
      
      Multipart multipart = new MimeMultipart();
      multipart.addBodyPart(messageBodyPart);
      //file attachment
      //messageBodyPart = new MimeBodyPart();
      //String filename = "file.txt";
      //DataSource source = new FileDataSource(filename);
      //messageBodyPart.setDataHandler(new DataHandler(source));
      //messageBodyPart.setFileName(filename);
      multipart.addBodyPart(messageBodyPart);
      message.setContent(multipart );
      
      Transport.send(message);
      String title = "Send Email";
      result = "Sent message successfully....";
   }catch (Exception ex) {
      ex.printStackTrace();
      result = "Error: unable to send message....";
   }
%>
<% 
   out.println("Result: " + result + "\n");
%>