package com.stillknow.common.domain;

import java.io.Serializable;

public class MngFile implements Serializable {
	
	private static final long serialVersionUID = 8348427598366340226L;
	
	private String companyid;
	private String fileid;
	private String filename;
	private long   filesize;
	private long   imagewidth;
	private long   imageheight;
	private String uploadpath;
	private String imagecell;
	private String filetype;
	private String filetype_cdft;
	private String description;
	private String tag;
	private String filehash;
	private String createdate;
	private String modifydate;
	
	
	private String targetId;
	
	private String newfileid;
	
	private String groupid;
	
	public String getcompanyid() {
		return companyid;
	}
	public void setcompanyid(String companyid) {
		this.companyid = companyid;
		System.out.println("12345");
	}

	public String getfileid() {
		return fileid;
	}
	public void setfileid(String fileid) {
		this.fileid = fileid;
	}
	
	public String getnewfileid() {
		return newfileid;
	}
	public void setnewfileid(String newfileid) {
		this.newfileid = newfileid;
	}
	
	public String getfilename() {
		return filename;
	}
	
	public void setfilename(String filename) {
		this.filename = filename;
	}
	
	public long getfilesize() {
		return filesize;
	}
	public void setfilesize(long filesize) {
		this.filesize = filesize;
	}
	
	public long getimagewidth() {
		return imagewidth;
	}
	public void setimagewidth(long imagewidth) {
		this.imagewidth = imagewidth;
	}
	
	public long getimageheight() {
		return imageheight;
	}
	public void setimageheight(long imageheight) {
		this.imageheight = imageheight;
	}
	
	public String getuploadpath() {
		return uploadpath;
	}
	public void setuploadpath(String uploadpath) {
		this.uploadpath = uploadpath;
	}
	
	public String getfiletype() {
		return filetype;
	}
	public void setfiletype(String filetype) {
		this.filetype = filetype;
	}
	
	public String getfiletype_cdft() {
		return filetype_cdft;
	}
	public void setfiletype_cdft(String filetype_cdft) {
		this.filetype_cdft = filetype_cdft;
	}
	
	public String getdescription() {
		return description;
	}
	public void setdescription(String description) {
		this.description = description;
	}
	
	public String gettag() {
		return tag;
	}
	public void settag(String tag) {
		this.tag = tag;
	}
	
	public String getgroupid() {
		return groupid;
	}
	public void setgroupid(String groupid) {
		this.groupid = groupid;
	}
	
	public String gettargetId() {
		return targetId;
	}
	public void settargetId(String targetId) {
		this.targetId = targetId;
	}
	
	public String getimagecell() {
		return imagecell;
	}
	public void setimagecell(String imagecell) {
		this.imagecell = imagecell;
	}
	
	public String getfilehash() {
		return filehash;
	}
	public void setfilehash(String filehash) {
		this.filehash = filehash;
	}
}
