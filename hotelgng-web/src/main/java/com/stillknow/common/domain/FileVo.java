package com.stillknow.common.domain;

import java.io.Serializable;

public class FileVo implements Serializable {
	
	private static final long serialVersionUID = 8348427598366340226L;
	
	private String refSeq;
	private String seq;
	private String fileName;
	private String fileType;
	private long   fileSize;
	private String filePath;
	private String urlPath;
	private String regId;
	private String targetId;

	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getRefSeq() {
		return refSeq;
	}
	public void setRefSeq(String refSeq) {
		this.refSeq = refSeq;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public long getFileSize() {
		return fileSize;
	}
	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getUrlPath() {
		return urlPath;
	}
	public void setUrlPath(String urlPath) {
		this.urlPath = urlPath;
	}
	public String gettargetId() {
		return targetId;
	}
	public void settargetId(String targetId) {
		this.targetId = targetId;
	}
	
}
