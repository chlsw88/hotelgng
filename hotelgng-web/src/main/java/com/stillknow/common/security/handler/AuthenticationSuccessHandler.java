package com.stillknow.common.security.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Component;

import com.stillknow.common.security.domain.LoginHistory;
import com.stillknow.common.security.service.LoginHistoryService;

/**
 * �α��� ���� �� ���� �̷��� ����� ���� �ڵ鷯
 * @author Aiden Kim
 *
 */
@Component
public class AuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    protected final Log logger = LogFactory.getLog(this.getClass());
	
	@Autowired
	public LoginHistoryService loginHistoryService;
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws ServletException, IOException {
		super.onAuthenticationSuccess(request, response, authentication);
		
		//User user = (User) authentication.getPrincipal();
		//LoginToken loginToken = (LoginToken) authentication.getPrincipal();
		//authentication.getPrincipal();
		
		// create login histroy instance
		LoginHistory lh = new LoginHistory();
		//lh.setLoginUser(loginToken.getUser());
		lh.setUser(authentication.getName());
		lh.setIpv4(request.getRemoteAddr());
		lh.setSessionId(request.getSession().getId());
		//lh.setLoginDate(new Date());
		//System.out.println("request.getSession().getId() :  "+ request.getSession().getId() );
		// regsiter -> db insert
		loginHistoryService.createLoginHist(lh);
		
		// set
		//loginToken.setHistory(lh);
	}
	
}
