package com.stillknow.common.security.session;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.session.SessionDestroyedEvent;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

import com.stillknow.common.security.domain.LoginHistory;
import com.stillknow.common.security.service.LoginHistoryService;

public class SessionDestroyListener implements ApplicationListener<SessionDestroyedEvent> {

	@Autowired
	private LoginHistoryService loginHistoryService;
	
	@Override
	public void onApplicationEvent(SessionDestroyedEvent event) {

		List<SecurityContext> contexts = event.getSecurityContexts();
		if (contexts.isEmpty() == false) {
			for (SecurityContext ctx : contexts) {
					
					/*
					System.out.println("ctx.getAuthentication().getDetails() : "+ctx.getAuthentication().getDetails());
					System.out.println("ctx.getAuthentication().getDetails() : "+webAuthenticationDetails.getSessionId());
					System.out.println("ctx.getAuthentication().getDetails() : "+webAuthenticationDetails.getRemoteAddress());
					System.out.println("event.getId() : "+event.getId());
					 */
					// login history
					WebAuthenticationDetails webAuthenticationDetails = (WebAuthenticationDetails) ctx.getAuthentication().getDetails();
					LoginHistory loginHistory = new LoginHistory();
					loginHistory.setIpv4(webAuthenticationDetails.getRemoteAddress());
					loginHistory.setSessionId(event.getId());
					//loginHistory.setSessionId(webAuthenticationDetails.getSessionId());
					loginHistory.setUser(ctx.getAuthentication().getName());
					//loginHistory.setLogoutDate(new Date());
					// update
					loginHistoryService.createLogoutHist(loginHistory);
					
					//break;
				// ...
			}
		}
	}
}


