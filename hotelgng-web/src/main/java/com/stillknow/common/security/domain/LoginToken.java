package com.stillknow.common.security.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;


@Configurable
public class LoginToken implements UserDetails {
	private static final long serialVersionUID = -2446413544111281211L;
	private LoginUser loginUser;
	private LoginHistory history;
	
	public LoginToken() {
		// TODO Auto-generated constructor stub
	}

	public LoginToken(LoginUser user) {
		this.loginUser = user;
	}

	public LoginHistory getHistory() {
		return history;
	}

	public void setHistory(LoginHistory loginHistory) {
		this.history = loginHistory;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<GrantedAuthority> list = new ArrayList<GrantedAuthority>();

		list.add(new GrantedAuthority() {
			private static final long serialVersionUID = -6572161678257321098L;

			@Override
			public String getAuthority() {
				return "ROLE_USER";
			}
		});

		return list;
	}

	@Override
	public String getPassword() {
		return loginUser.getPassword();
	}

	@Override
	public String getUsername() {
		return loginUser.getUsername();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public LoginUser getUser() {
		return loginUser;
	}

	public void setUser(LoginUser user) {
		this.loginUser = user;
	}

	@Override
	public String toString() {
		return "LoginToken [user=" + loginUser + ", history=" + history + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((loginUser == null) ? 0 : loginUser.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LoginToken other = (LoginToken) obj;
		if (loginUser == null) {
			if (other.loginUser != null)
				return false;
		} else if (!loginUser.equals(other.loginUser))
			return false;
		return true;
	}

}
