package com.stillknow.common.security.persistence;
import java.util.List;

import com.stillknow.common.global.utils.DataMap;
import com.stillknow.common.security.domain.LoginHistory;

public interface LoginHistoryMapper {
		
	public List<LoginHistory> selectLoginHistory();
	public List<LoginHistory> selectLoginHistory(DataMap parameters);
	public void createLoginHist(LoginHistory loginHistory);
	public void createLogoutHist(LoginHistory loginHistory);

}
