package com.stillknow.common.security.web;


import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import javax.management.InstanceAlreadyExistsException;
import javax.servlet.ServletContext;

import org.aspectj.weaver.ast.Instanceof;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
//import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.stillknow.common.global.utils.DataMap;
import com.stillknow.common.global.utils.FileUtil;
import com.stillknow.common.global.utils.StringUtils;
import com.stillknow.common.security.domain.CustomUser;

@Controller
@RequestMapping(value="/common/login/*")
public class LoginController {
	
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	private ApplicationContext applicationContext;
	
	@Autowired
	private ServletContext servletContext;
	
	/*
	@RequestMapping
	public ModelAndView 
	
	*/

	
	/* 濡쒓렇���⑤뱾�� */
	@RequestMapping
	public ModelAndView login() throws Exception {
		
		//ROLE_ANONYMOUS, anonymousUser
		RedirectView rdrv = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		logger.debug(" principal instanceof String ");
		logger.debug("/common/login/login.do ::: is empty principal ="+ principal );
		logger.debug(" principal instanceof CustomUser ");
		if( principal instanceof String ){
			if( !StringUtils.isEmpty(principal) && "anonymousUser".equals(principal) ){
				rdrv = new RedirectView("/html/common/login/login_failure.html");
			} 
		} else if ( principal instanceof CustomUser ){
			CustomUser tempUser =  (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			logger.debug("/common/login/login.do ::: user.getUsername() ="+ tempUser.getUsername() );
			logger.debug("/common/login/login.do ::: user.getUserSeq() ="+ tempUser.getUserSeq() );
			if( !StringUtils.isEmpty(tempUser.getUsername()) /* && !StringUtils.isEmpty(tempUser.getCompanyCd()) */ ){
				logger.debug("/common/login/login.do ::: is not  empty principal ="+ tempUser.getUsername() );
				logger.debug("/common/login/login.do ::: dddddddddd" );
				rdrv = new RedirectView("/html/common/login/login_success.html");
			}
		}
		logger.debug("/common/login/login.do ::: principal ="+ principal );
		logger.debug("/common/login/login.do ::: principal ="+ principal );
		int result = 0;
		if( rdrv == null ){
			rdrv = new RedirectView("/html/common/login/login_failure.html");
		}
		return new ModelAndView(rdrv);
	}
	
}
