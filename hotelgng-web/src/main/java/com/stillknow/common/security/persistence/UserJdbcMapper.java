package com.stillknow.common.security.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityCoreVersion;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;

import com.stillknow.common.domain.Crc32;
import com.stillknow.common.security.domain.CustomUser;

public class UserJdbcMapper extends JdbcDaoImpl implements UserDetailsService {
	private static final long serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID;
	private static final Logger logger = LoggerFactory.getLogger(UserJdbcMapper.class);

	@Override
	public UserDetails loadUserByUsername(String username1) {
		final String username2 = username1;
        //String crcCode = String.valueOf(new Crc32().getCode(username1));
        logger.debug("loadUsersByUsername(String username) : " + username1);
        //UsernamePasswordAuthenticationFilter.class.getMethod(name, parameterTypes);
        List<CustomUser> userList = getJdbcTemplate().query( getUsersByUsernameQuery(), new String[] {username1}, new RowMapper<CustomUser>() {
	        public CustomUser mapRow(ResultSet rs, int rowNum) throws SQLException {
	            String username = rs.getString(1);
	            logger.debug("loadUsersByUsername : mapRow : username : " + username);
	            String password = rs.getString(2);
	            logger.debug("loadUsersByUsername : mapRow : password : " + password);
	            String userSeq = rs.getString(3);
	            logger.debug("loadUsersByUsername : mapRow : userSeq : " + userSeq);
	            boolean enabled = rs.getInt(4) == 1 ? true : false;
	            logger.debug("loadUsersByUsername : mapRow : enabled : " + enabled);
	            logger.debug("loadUsersByUsername : mapRow : AuthorityUtils.NO_AUTHORITIES : " + AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER,ROLE_ADMIN"));
	            logger.debug("username2 ::: " + username2); 
	            @SuppressWarnings("unchecked")
				CustomUser customUser =  new CustomUser(username, password, userSeq, enabled, true, true, true, 
	            		(List<GrantedAuthority>) getJdbcTemplate().query( getAuthoritiesByUsernameQuery(), new String[] {username2}, new RowMapper<GrantedAuthority>(){
										@Override
										public GrantedAuthority mapRow(ResultSet rs, int rowNum) throws SQLException {
											return new GrantedAuthorityImpl(rs.getString("authority"));
										}
	            					}
	            				)
	            		); 
	            logger.debug("mapRow : getUsername : " + customUser.getUsername());
	            logger.debug("mapRow : getPassword : " + customUser.getPassword());
	            logger.debug("mapRow : isEnabled   : " + customUser.isEnabled());
	            
	            return customUser; 
	            }
        }); 
        return userList.get(0);
    }
/*
	private Collection<? extends GrantedAuthority> getUserAuthorities(Users authorities) {

		int size = authorities.getAuthority().size();
		List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>(size);
		for (int idx = 0; idx < size; idx++) {
			authList.add(new GrantedAuthorityImpl(authorities.getAuthority().get(idx)));
		}
		return authList;
	}
	*/

} 