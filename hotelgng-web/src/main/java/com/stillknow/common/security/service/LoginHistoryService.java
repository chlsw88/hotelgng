package com.stillknow.common.security.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stillknow.common.global.utils.DataMap;
import com.stillknow.common.security.domain.LoginHistory;
import com.stillknow.common.security.persistence.LoginHistoryMapper;

@Service("loginHistoryService")
public class LoginHistoryService {
		
	public LoginHistoryService() {
		// TODO Auto-generated constructor stub
		//System.out.println("123123123123123");
	}
	
	@Autowired
	private LoginHistoryMapper loginHistoryMapper;

	public List<LoginHistory> selectLoginHistory(){
		return null;
	}
	
	public List<LoginHistory> selectLoginHistory(DataMap parameters){
		return null;
	}
	
	public void createLoginHist(LoginHistory loginHistory) {
		loginHistoryMapper.createLoginHist(loginHistory);
	}
	
	public void createLogoutHist(LoginHistory loginHistory) {
		loginHistoryMapper.createLogoutHist(loginHistory);
	}

	
}
