package com.stillknow.common.scheduling;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Clob;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.stillknow.common.global.utils.DataMap;
import com.stillknow.common.scheduling.persistence.GcmMapper;

public class RunMeTask {
	
	@Autowired
	private GcmMapper gcmMapper;
	
	public void printMe() {
		System.out.println("Spring 3 + Quartz 1.8.6 ~");
		sendInfo();
		getInfoList();
	}
	public List<DataMap> getInfoList(){
		DataMap dataMap = new DataMap();
		List<DataMap> infoList = gcmMapper.selectInfo(dataMap);
		System.out.println(infoList);
		return infoList;
	}
	public void sendInfo(){
		String API_KEY = "AIzaSyDMXAX_s6YkbzgEVKIiN_njZRRxgV6gHXs";
		//[0] = message, [1] = reciever
		String[] args = {"",""};
		//args[0] = request.getParameter("msg") != null ? URLDecoder.decode(request.getParameter("msg"),"UTF-8") : "" ;
		//args[1] = request.getParameter("to") != null ? request.getParameter("to") : "eRL26a4bWGU:APA91bE0cj18y5s4mjg-vdB17clrGi6w-wDTlsU3JiOpTO_6h1_qQiH3w8Q8rxUlYCDgHD1DsbSJQjnVjywcrNnq_5B7Nk5KdK-LVatRFVAaV80X-RNIDFii2lScyDedQ01TDCUkXF24" ;
		
		List<DataMap> infoList = getInfoList();
//		for(int i=0; i < infoList.size(); i++){
		for(int i=0; i < 1; i++){
			DataMap dataMap = infoList.get(i);
			//args[0] = dataMap.getString("TEXT").toString();
			args[0] = "가치 있는 앱 만들기. 100억 벌기.";
			args[1] = "dLOtbAX4JwQ:APA91bEa3mGwnTxzIVvpHFisPlydAj9YsOxMaTV45979ArF38JFyJLw7o-7dCJNoAfFUXDnQtWD-I3-ookqyg5XTGLHsE1KI4vlM2aNw4tjJqI4cRtpjvd_rVSMtLo7V9wvXIIxfVOti";
			if(!"".equals(args[0])){
			    try {
			        // Prepare JSON containing the GCM message content. What to send and where to send.
			        JsonObject jGcmData = new JsonObject();
			        JsonObject jData = new JsonObject();
			        jData.addProperty("message", URLEncoder.encode(args[0].trim(),"UTF-8"));
			        // Where to send GCM message.
			        if (args.length > 1 && args[1] != null) {
			            jGcmData.addProperty("to", args[1].trim());
			        } else {
			            jGcmData.addProperty("to", "/topics/global");
			        }
			        // What to send in GCM message.
			        jGcmData.add("data", jData);
			
			        // Create connection to send GCM Message request.
			        URL url = new URL("https://android.googleapis.com/gcm/send");
			        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			        conn.setRequestProperty("Authorization", "key=" + API_KEY);
			        conn.setRequestProperty("Content-Type", "application/json");
			        conn.setRequestMethod("POST");
			        conn.setDoOutput(true);
			
			        // Send GCM message content.
			        OutputStream outputStream = conn.getOutputStream();
			        outputStream.write(jGcmData.toString().getBytes());
			
			        // Read GCM response.
			        InputStream inputStream = conn.getInputStream();
			        String resp = IOUtils.toString(inputStream);
			        System.out.println(resp);
			        
			        dataMap.put("userSeq", "1");
			        dataMap.put("infoSeq", "1");
			        
			        if(resp.indexOf("\"success\":1") > -1){
			        	dataMap.put("resultCd", "1"); //����
			        } else {
			        	dataMap.put("resultCd", "0"); //����
			        }
			        gcmMapper.insertNtfcLog(dataMap);
			        
			    } catch (IOException e) {
			        System.out.println("Unable to send GCM message.");
			        System.out.println("Please ensure that API_KEY has been replaced by the server " +
			                "API key, and that the device's registration token is correct (if specified).");
			        e.printStackTrace();
			    }
			}
		        //new Gson().fromJson(resp, new TypeToken<List<HashMap>>(){}.getType());
		        //HashMap map = new Gson().fromJson(resp, new TypeToken<Map>(){}.getType());
		        System.out.println("Check your device/emulator for notification or logcat for " +
		                "confirmation of the receipt of the GCM message.");
		}

	}
}