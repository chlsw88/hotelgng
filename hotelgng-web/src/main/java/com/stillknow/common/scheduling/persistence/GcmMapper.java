package com.stillknow.common.scheduling.persistence;
import java.util.List;

import com.stillknow.common.global.utils.DataMap;

public interface GcmMapper {
		
	/*
	public List<LoginHistory> selectLoginHistory();
	public List<LoginHistory> selectLoginHistory(DataMap parameters);
	public void createLoginHist(LoginHistory loginHistory);
	public void createLogoutHist(LoginHistory loginHistory);
	 */
	public List<DataMap> selectInfo(DataMap parameters);
	public void insertNtfcLog(DataMap parameters);
}
