package com.stillknow.common.global.utils;

import com.google.protobuf.ByteString;
import com.mongodb.DBCollection;

/**
 * �̹��� ������ ��
 * 
 * 1. ��������
 * 2. ��ü���� ���Ǵ� �̹��� (����,��ǰ, ��Ÿ���� ��)
 * 3. �������� (�ʿ��)
 * @author k0mo
 *
 */
public interface IImageDataManageLib {

	// �ܺ� �̹��� �� �������̽�
	public void saveImage(DBCollection imageDBCollection, String userId, String imageUniqueName, byte[] pictureDataBin) throws Exception;
	public void removeImage(DBCollection imageDBCollection, String imageHashcode) throws Exception;
//	public String getImage(DBCollection imageDBCollection, String imageHashcode) throws Exception;
	public byte[] getImage(DBCollection imageDBCollection, String imageHashcode) throws Exception;
	
}
