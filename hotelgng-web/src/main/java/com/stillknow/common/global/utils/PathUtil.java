package com.stillknow.common.global.utils;

import java.io.File;






public final class PathUtil
{
 
    public PathUtil()
    {
    }

    public static String toCurrentPath(String path)
    {
        String cPath = path;
        if(SystemUtil.IS_OS_WINDOWS)
            cPath = toWindowsPath(cPath);
        else
            cPath = toUnixPath(cPath);
        File file1 = new File(cPath);
        File file2 = new File(file1.getAbsolutePath());
        if(file2.exists())
            cPath = file2.getAbsolutePath();
        file1 = null;
        return cPath.trim();
    }

    public static String toUnixPath(String inPath)
    {
        StringBuffer path = new StringBuffer();
        int index = -1;
        inPath = inPath.trim();
        index = inPath.indexOf(":\\");
        inPath = inPath.replace('\\', '/');
        if(index > -1)
        {
            path.append('/');
            path.append(inPath.substring(index + 2));
        } else
        {
            path.append(inPath);
        }
        return path.toString();
    }

    public static String toWindowsPath(String path)
    {
        String winPath = path;
        int index = winPath.indexOf("//");
        if(index > -1)
            winPath = (new StringBuilder()).append(winPath.substring(0, index)).append(":\\").append(winPath.substring(index + 2)).toString();
        index = winPath.indexOf(':');
        if(index == 1)
            winPath = (new StringBuilder()).append(winPath.substring(0, 1).toUpperCase()).append(winPath.substring(1)).toString();
        winPath = winPath.replace('/', '\\');
        return winPath;
    }

    public static String dirName(String dirname)
    {
        int i = dirname.lastIndexOf(File.separator);
        return i < 0 ? dirname : dirname.substring(0, i);
    }

    public static String fileName(String filename)
    {
        int i = filename.lastIndexOf(File.separator);
        return i < 0 ? filename : filename.substring(i + 1);
    }

    public static String baseName(String filename)
    {
        return baseName(filename, extension(filename));
    }

    public static String baseName(String filename, String suffix)
    {
        int i = filename.lastIndexOf(File.separator) + 1;
        int lastDot = suffix == null || suffix.length() <= 0 ? -1 : filename.lastIndexOf(suffix);
        if(lastDot >= 0)
            return filename.substring(i, lastDot - 1);
        if(i > 0)
            return filename.substring(i - 1);
        else
            return filename;
    }

    public static String extension(String filename)
    {
        int lastDot = filename.lastIndexOf('.');
        if(lastDot >= 0)
            return filename.substring(lastDot + 1);
        else
            return "";
    }
}
