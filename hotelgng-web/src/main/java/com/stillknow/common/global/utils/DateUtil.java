package com.stillknow.common.global.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.time.FastDateFormat;
//import org.springframework.util.StringUtils;

public class DateUtil {
	
	/**
	 * yyyyMMddHHmmssSSS
	 * @return String
	 */
	synchronized public static String getLogTime() {
		
		FastDateFormat fastDateFormat = FastDateFormat.getInstance("yyyyMMddHHmmssSSS");
		return fastDateFormat.format(new Date());
	}
	 
	/**
	 * yyyyMMdd
	 * @return String
	 */
	public static String getYYYYMMDD() {
		
		FastDateFormat fastDateFormat = FastDateFormat.getInstance("yyyyMMdd");
		return fastDateFormat.format(new Date());
	}
	/**
	 * yyyyMMdd
	 * @return String
	 */
	public static String getYYMMDD() {
		
		FastDateFormat fastDateFormat = FastDateFormat.getInstance("yyMMdd");
		return fastDateFormat.format(new Date());
	}
	
	/**
	 * yyyyMMdd
	 * @return String
	 */
	public static Date getDateFromYYYYMMDD( String dateString ) {
		
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd"); 
		
		Date date = null;
		try {
			date = sdf1.parse(dateString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return date;
	}
	
	public static Date addMonths(Date aDate, int number){
	    Calendar aCalendar = Calendar.getInstance();  
	    aCalendar.setTime(aDate);  
	    aCalendar.add(Calendar.MONTH, number);  
	    return aCalendar.getTime();  
	}
	
	/**
	 * 
	 * @param aDate
	 * @param number
	 * @param type : y(year), m(month), d(day)
	 * @return calculated date
	 */
	public static Date addDate(Date aDate, int number, String type){ 
		Calendar aCalendar = Calendar.getInstance();  
		aCalendar.setTime(aDate);
		
		if("d".equals(type)){
			aCalendar.add(Calendar.DATE, number);  
		} else if ("m".equals(type)){
			aCalendar.add(Calendar.MONTH, number);  
		} else if ("y".equals(type)){
			aCalendar.add(Calendar.YEAR, number);  
		}
		
		return aCalendar.getTime();  
	}
	
	/**
	 * yyyyMM
	 * @return String
	 */
	public static String getYYYYMM() {
		
		FastDateFormat fastDateFormat = FastDateFormat.getInstance("yyyyMM");
		return fastDateFormat.format(new Date());
	}
	
	public static String getYYMM() {
		
		FastDateFormat fastDateFormat = FastDateFormat.getInstance("yyMM");
		return fastDateFormat.format(new Date());
	}
	
	
	/**
	 * yyyy
	 * @return String
	 */
	public static String getYYYY() {
		
		FastDateFormat fastDateFormat = FastDateFormat.getInstance("yyyyMM");
		return fastDateFormat.format(new Date());
	}
	
	
	/**
	 * MM
	 * @return String
	 */
	public static String getMM() {
		
		FastDateFormat fastDateFormat = FastDateFormat.getInstance("yyyyMM");
		return fastDateFormat.format(new Date());
	}
	
	/**
	 * yyyyMM
	 * @return String
	 */
	public static String getYYYYMM(Date date) {
		
		FastDateFormat fastDateFormat = FastDateFormat.getInstance("yyyyMM");
		return fastDateFormat.format(date);
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	public static String getYYYYMMDD(Date date) {
		
		FastDateFormat fastDateFormat = FastDateFormat.getInstance("yyyyMMdd");
		return fastDateFormat.format(date);
	}
	/**
	 * 
	 * @param date
	 * @param format : 'yyyy', 'yyyymm', 'yyyymmdd'
	 * @return
	 */
	public static String getDateToString(Date date, String _format) {
		String format = "yyyyMMdd";
		if( StringUtils.isEmpty(format) ){
		
		} else if( !_format.contains("yyyy") && !_format.contains("mm") && !_format.contains("dd") ) {
			
		} else {
			format = _format.replaceAll("mm", "MM");
		}
		
		FastDateFormat fastDateFormat = FastDateFormat.getInstance("yyyyMMdd");
		return fastDateFormat.format(date);
	}
}
