package com.stillknow.common.global.utils;

import java.net.UnknownHostException;

import org.apache.log4j.Logger;


import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;
import com.mongodb.MongoException;

public class MongoDBFactory {
	protected static Logger logger = 
	  Logger.getLogger(MongoDBFactory.class);
	
	private static Mongo m;
	
	private MongoDBFactory() {}

	public static Mongo getMongo() {
		if (m == null) {
			try {
				m = new Mongo( "163.180.56.25" , 27017 );
			} catch (UnknownHostException e) {
				logger.error(e);
			} catch (MongoException e) {
				logger.error(e);
			}
		}
		
		return m;
	}
	
	public static DB getDB(String dbname) {
		return getMongo().getDB(dbname);
	}
	
	// Retrieve a collection
	public static DBCollection getCollection(String dbname, 
	   String collection) {
		return getDB(dbname).getCollection(collection);
	}
}