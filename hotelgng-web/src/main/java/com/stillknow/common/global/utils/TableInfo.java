package com.stillknow.common.global.utils;

import java.util.ArrayList;
import java.util.List;

public class TableInfo {

	public String tableName;
	public List<String> pkList;
	
	public List<String> columnList;
	
	public TableInfo(String tableName)
	{
		this.tableName = tableName;
		pkList = new ArrayList<String>();
		columnList = new ArrayList<String>();
	}

	private String insertPreSql;
	private String updatePreSql;
	private String deletePreSql;
	
	public String getSql(DmlType dmlType)
	{
		if(dmlType == DmlType.INSERT)
		{
			if(insertPreSql == null)
			{
				insertPreSql = "INSERT INTO " + tableName + "\n";
				insertPreSql += "( ";
				for(int colIndex=0;colIndex<columnList.size();colIndex++)
				{
					if(colIndex != 0)
						insertPreSql += ", ";
					insertPreSql += columnList.get(colIndex);
				}
				insertPreSql += ") VALUES (\n";
				for(int colIndex=0;colIndex<columnList.size();colIndex++)
				{
					if(colIndex != 0)
						insertPreSql += ", ";
					insertPreSql += "?";
				}
				insertPreSql += ")\n";
			}
			return insertPreSql;
		}
		else if(dmlType == DmlType.UPDATE)
		{
			if(updatePreSql == null)
			{
				updatePreSql = " UPDATE " + tableName + " SET\n";
				for(int colIndex=0;colIndex<columnList.size();colIndex++)
				{
					if(colIndex != 0)
						updatePreSql += " , ";
					updatePreSql += columnList.get(colIndex) + " = ?\n";
				}
				updatePreSql += " WHERE ";
				for(int colIndex=0;colIndex<pkList.size();colIndex++)
				{
					if(colIndex != 0)
						updatePreSql += " AND ";
					updatePreSql += pkList.get(colIndex) + " = ?\n";
				}
			}
			return updatePreSql;
		}
		else if(dmlType == DmlType.DELETE)
		{
			if(deletePreSql == null)
			{
				deletePreSql = " DELETE FROM " + tableName + "\n";
				deletePreSql += " WHERE ";
				for(int colIndex=0;colIndex<pkList.size();colIndex++)
				{
					if(colIndex != 0)
						deletePreSql += " AND ";
					deletePreSql += pkList.get(colIndex) + " = ?\n";
				}
			}
			return deletePreSql;
		}
		return null;
	}
	
	public String toString()
	{
		String out = "TABLE : " + tableName + "\n";
		
		for(String pk : pkList)
		{
			out += pk + " ";
		}
		
		out += "\n";
		
		for(String column : columnList)
		{
			out += column + " ";
		}
		
		out += "\n";
		
		return out;
	}
}
