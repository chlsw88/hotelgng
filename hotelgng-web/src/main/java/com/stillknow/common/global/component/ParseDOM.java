package com.stillknow.common.global.component;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * @author SubongKim
 * @create : 2014. 03. 07.
 */
public class ParseDOM {

	private final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	private DocumentBuilder builder;

	public ParseDOM() throws ParserConfigurationException {
		builder = factory.newDocumentBuilder();
	}

	public void parseDOM(String xmlUri) {
		
		try {
			Document document = builder.parse(xmlUri);
			NodeList nodeList = document.getChildNodes();
			int nodeLength = nodeList.getLength();
			for (int i = 0; i < nodeLength; i++) {
				printNodeInfo(nodeList.item(i));
			}
		} catch (SAXException e) {
			
		} catch (IOException e) {
			
		}
		
	}

	/**
	 * @param item
	 */
	private void printNodeInfo(Node node) {
		
		String value = "child node";
		short childType = getChildType(node);
		
		if (childType == Node.TEXT_NODE) {
			//value = node.getTextContent();
		}
		
		StringBuilder sb = new StringBuilder(200);
		sb.append("node name:").append(node.getNodeName())
				.append(", node value:").append(value);
		
		if (node.hasAttributes()) {
			NamedNodeMap attributeMap = node.getAttributes();
			int attributeLength = attributeMap.getLength();
			for (int i = 0; i < attributeLength; i++) {
				Node attNode = attributeMap.item(i);
				sb.append('\n').append("attr name:")
						.append(attNode.getNodeName()).append(", attr value:")
						.append(attNode.getNodeValue());
			}
		}
		
		System.out.println(sb);
		
		if (node.hasChildNodes()) {
			NodeList nodeList = node.getChildNodes();
			int nodeLength = nodeList.getLength();
			
			for (int i = 0; i < nodeLength; i++) {
				Node childNode = nodeList.item(i);
				
				if (childNode.getNodeType() == Node.TEXT_NODE) {
					continue;
				}
				printNodeInfo(nodeList.item(i));
			}
		}
		
	}

	/**
	 * @param node
	 * @return
	 */
	private short getChildType(Node node) {
		
		NodeList nodeList = node.getChildNodes();
		int length = nodeList.getLength();
		for (int i = 0; i < length; i++) {
			Node childNode = nodeList.item(i);
			if (childNode.getNodeType() != Node.TEXT_NODE)
				return childNode.getNodeType();
		}
		return Node.TEXT_NODE;
	}

}	
