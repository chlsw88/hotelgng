package com.stillknow.common.global.utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.stillknow.common.security.domain.CustomUser;
import com.stillknow.lifter.model.Content;
import com.stillknow.lifter.model.Lifter;
import com.stillknow.lifter.model.Phone;

/**
 * @author	: Administrator
 * @date	: 2016. 5. 18. 오후 2:07:52
 * @project : lifter
 * @package : com.stillknow.common.global.utils
 * @file	: DataTableUtil.java
 */
/**
 * @author	: Administrator
 * @date	: 2016. 5. 26. 오전 11:45:17
 * @project : lifter
 * @package : com.stillknow.common.global.utils
 * @file	: DataTableUtil.java
 */
public class DataTableUtil {

	public static final String WEB_DATA_TABLE_EDIT_TAG = "<a class='edit' href='javascript:;'> 수정 </a>";
	public static final String WEB_DATA_TABLE_DELETE_TAG = "<a class='delete' style='color: chocolate;' href='javascript:;'> 삭제 </a>";
	public static final String WEB_DATA_TABLE_X_TAG = "<span class='label label-sm label-default'> Blocked </span>";
	public static List putListWithAuth(List<Phone> list, CustomUser user){
		for(Lifter lifter : list){
			lifter.setEDIT(WEB_DATA_TABLE_EDIT_TAG);
			lifter.setDELETE(WEB_DATA_TABLE_DELETE_TAG);
		}
		return list;
	}
	public static List putListContentWithAuth(List<Content> list, CustomUser user){
		for(Lifter lifter : list){
			lifter.setEDIT(WEB_DATA_TABLE_EDIT_TAG);
			lifter.setDELETE(WEB_DATA_TABLE_DELETE_TAG);
		}
		return list;
	}
	public static String getSeqKeyFromString(String str){
		String returnStr = "";
		if(str!=null && !"".equals(str)){
			returnStr=str.substring(str.indexOf("[")+1, str.indexOf("]"));
		}
		return returnStr;
	}
	public static String getColumnNameFromString(String str){
		String returnStr = "";
		if(str!=null && !"".equals(str)){
			returnStr=str.substring(str.indexOf("[", str.indexOf("[")+1)+1,str.indexOf("]",str.indexOf("]")+1));
		}
		return returnStr;
	}
	/**
	 * @param parameters
	 * @param strings
	 * @return
	 * 
	 */
	public static <K> String getParamsAsJson(DataMap parameters, String[] strArray){
		/* data table 값 형태 */
		/*
		{
		(data[27][PAGE_RANK],3)(data[25][CONTENT_TITLE],노원 맛집 무한으로 ㅂH터ㅈl게 식사하셔요 !)
		(data[27][CONTENT_URL],http://m.blog.naver.com/ddyt29/220698372706)
		(data[27][DELAY_TM],15)(data[27][CONTENT_TITLE],남부터미널 맛집 제일 잘나갈비 !)
		(data[27][DELAY_RANGE],3)(data[25][PRIOR],)(data[27][PRIOR],)(data[25][DELAY_RANGE],3)
		(data[25][CONTENT_URL],http://m.blog.naver.com/popozl/220701885281)(data[25][USE_YN],Y)
		(data[27][CONTENT_KEYWORD],남부터미널 맛집)(data[25][PAGE_RANK],15)(data[25][CONTENT_KEYWORD],노원 맛집)
		(action,edit)(data[25][PAGE_NUM],7)(data[25][DELAY_TM],15)(data[27][USE_YN],Y)(data[27][PAGE_NUM],5)
		}
		 */
		boolean action=false;
		String key;
		String val;

		Iterator<K> it = parameters.keySet().iterator();
//		Content contentA = new Content();
//		String paramVal = "";
		
//		Map<String,HashMap> keyValMap = new HashMap<String,HashMap>();
		JsonObject keyJsonObj = new JsonObject();
		while( it.hasNext() ){
			String seqKey = "";
			key = (String) it.next();
			val = parameters.getString( key );
			if(key.contains("data")){
				seqKey=DataTableUtil.getSeqKeyFromString(key);
				if(keyJsonObj.has(seqKey)){
					if(!StringUtils.isEmpty(val)){
						
					}
					keyJsonObj.getAsJsonObject(seqKey).addProperty(DataTableUtil.getColumnNameFromString(key), val); 					
				}else{
					if(!StringUtils.isEmpty(val)){
					}
					keyJsonObj.add(seqKey, new JsonObject());
					keyJsonObj.getAsJsonObject(seqKey).addProperty(strArray[0], seqKey);
					keyJsonObj.getAsJsonObject(seqKey).addProperty(DataTableUtil.getColumnNameFromString(key), val);
				}
				
				/*
				if(keyValMap.containsKey(seqKey)){
					keyValMap.get(seqKey).put(DataTableUtil.getColumnNameFromString(key), val);
				} else {
					keyValMap.put(seqKey, new HashMap<String,String>());
					keyValMap.get(seqKey).put("CONTENT_SEQ", seqKey);
					keyValMap.get(seqKey).put(DataTableUtil.getColumnNameFromString(key), val);
				}
				*/
					
//				System.out.println(DataTableUtil.getSeqKeyFromString(key));
//				System.out.println(DataTableUtil.getColumnNameFromString(key));
//				System.out.println("paramVal ::: "+ paramVal);
			}
			if("action".equals(key) && ("create".equals(val) || "edit".equals(val) || "remove".equals(val))) action=true;
		}
//		System.out.println("keyValMap ::: " + keyValMap.toString());
//		System.out.println("keyJsonObj ::: " + keyJsonObj.toString());
		Iterator<Entry<String, JsonElement>> itr = keyJsonObj.entrySet().iterator();
		JsonArray jsonArray = new JsonArray();
		while( itr.hasNext() ){
			jsonArray.add(itr.next().getValue());
			//keyJsonObj.remove(key);
		}
//		keyJsonObj = new JsonObject();
//		keyJsonObj.add("CONTENT", jsonArray);
//		System.out.println("keyJsonObj ::: " + keyJsonObj.toString());
		return action?jsonArray.toString():"[]";
	}
}
