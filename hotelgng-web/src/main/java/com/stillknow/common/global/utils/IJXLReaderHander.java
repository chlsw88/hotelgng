package com.stillknow.common.global.utils;

import java.util.Map;

public interface IJXLReaderHander {

	/**
	 * ������ DMLó�� ��� ���� ���Ͽ� ǥ������ ����
	 * @return
	 */
	public boolean isResultMark();
	
	/**
	 * �ϳ��� ���� �о��� �� ȣ��Ǵ� �޼ҵ�
	 * @param tableInfo
	 * @param dmlType
	 * @param dataMap
	 * @return
	 * @throws Exception
	 */
	public boolean rowReaded(TableInfo tableInfo, DmlType dmlType, Map<String, String> dataMap) throws Exception;
	
}
