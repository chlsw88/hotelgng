package com.stillknow.common.global.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

public final class FileUtil {

	public FileUtil() {
	}

	public static String getWorkingDir() {
		return System.getProperty("user.dir");
	}

	public static File getWorkingDirFile() {
		return new File(getWorkingDir());
	}

	public static long getFileSize(String fileName) {
		return getFileSize(new File(fileName));
	}

	public static long getFileSize(File file) {
		return file.length();
	}

	public static boolean equals(String file1, String file2) {
		return equals(new File(file1), new File(file2));
	}

	public static boolean equals(File file1, File file2) {
		try {
			file1 = file1.getCanonicalFile();
			file2 = file2.getCanonicalFile();
		} catch (IOException e) {
			return false;
		}
		return file1.equals(file2);
	}

	public static boolean mkdirs(String dirs) {
		return mkdirs(new File(dirs));
	}

	public static boolean mkdirs(File dirs) {
		return dirs.mkdirs();
	}

	public static boolean mkdir(String dir) {
		return mkdir(new File(dir));
	}

	public static boolean mkdir(File dir) {
		return dir.mkdir();
	}

	public static boolean copy(String fileIn, String fileOut) {
		return copy(new File(fileIn), new File(fileOut), FILE_BUFFER_SIZE, true);
	}

	public static boolean copySafe(String fileIn, String fileOut) {
		return copy(new File(fileIn), new File(fileOut), FILE_BUFFER_SIZE,
				false);
	}

	public static boolean copy(String fileIn, String fileOut, int bufsize) {
		return copy(new File(fileIn), new File(fileOut), bufsize, true);
	}

	public static boolean copySafe(String fileIn, String fileOut, int bufsize) {
		return copy(new File(fileIn), new File(fileOut), bufsize, false);
	}

	public static boolean copy(File fileIn, File fileOut) {
		return copy(fileIn, fileOut, FILE_BUFFER_SIZE, true);
	}

	public static boolean copySafe(File fileIn, File fileOut) {
		return copy(fileIn, fileOut, FILE_BUFFER_SIZE, false);
	}

	public static boolean copy(File fileIn, File fileOut, int bufsize) {
		return copy(fileIn, fileOut, bufsize, true);
	}

	public static boolean copySafe(File fileIn, File fileOut, int bufsize) {
		return copy(fileIn, fileOut, bufsize, false);
	}

	public static boolean copy(String fileIn, String fileOut, int bufsize,
			boolean overwrite) {
		return copy(new File(fileIn), new File(fileOut), bufsize, overwrite);
	}

	public static boolean copy(File fileIn, File fileOut, int bufsize,
			boolean overwrite) {
		if (!fileIn.exists())
			return false;
		if (!fileIn.isFile())
			return false;
		if (fileOut.isDirectory())
			fileOut = new File((new StringBuilder()).append(fileOut.getPath())
					.append(File.separator).append(fileIn.getName()).toString());
		if (!overwrite) {
			if (fileOut.exists())
				return false;
		} else if (fileOut.exists())
			try {
				if (fileIn.getCanonicalFile()
						.equals(fileOut.getCanonicalFile()))
					return true;
			} catch (IOException ioex) {
				return false;
			}
		return copyFile(fileIn, fileOut, bufsize);
	}

	public static boolean copyFile(String fileIn, String fileOut) {
		return copyFile(new File(fileIn), new File(fileOut), FILE_BUFFER_SIZE);
	}

	public static boolean copyFile(File fileIn, File fileOut) {
		return copyFile(fileIn, fileOut, FILE_BUFFER_SIZE);
	}

	public static boolean copyFile(String fileIn, String fileOut, int bufsize) {
		return copyFile(new File(fileIn), new File(fileOut), bufsize);
	}

	public static boolean copyFile(File fileIn, File fileOut, int bufsize) {
		FileChannel sourceChannel;
		FileChannel destinationChannel;
		boolean result;
		sourceChannel = null;
		destinationChannel = null;
		result = false;
		try {
			sourceChannel = (new FileInputStream(fileIn)).getChannel();
			destinationChannel = (new FileOutputStream(fileOut)).getChannel();
			sourceChannel.transferTo(0L, sourceChannel.size(),
					destinationChannel);
			result = true;
		} catch (IOException ioex) {
			if (sourceChannel != null)
				try {
					sourceChannel.close();
				}
				// Misplaced declaration of an exception variable
				catch (IOException ioexx) {
				}
			if (destinationChannel != null)
				try {
					destinationChannel.close();
				}
				// Misplaced declaration of an exception variable
				catch (IOException ioexxx) {
				}

		}
		if (sourceChannel != null)
			try {
				sourceChannel.close();
			} catch (IOException ioex) {
			}
		if (destinationChannel != null)
			try {
				destinationChannel.close();
			} catch (IOException ioex) {
			}

		if (sourceChannel != null)
			try {
				sourceChannel.close();
			} catch (IOException ioex) {
			}
		if (destinationChannel != null)
			try {
				destinationChannel.close();
			} catch (IOException ioex) {
			}

		return result;
	}

	public static boolean move(String fileNameIn, String fileNameOut) {
		return move(new File(fileNameIn), new File(fileNameOut), true);
	}

	public static boolean moveSafe(String fileNameIn, String fileNameOut) {
		return move(new File(fileNameIn), new File(fileNameOut), false);
	}

	public static boolean move(File fileIn, File fileOut) {
		return move(fileIn, fileOut, true);
	}

	public static boolean moveSafe(File fileIn, File fileOut) {
		return move(fileIn, fileOut, false);
	}

	public static boolean move(String fileNameIn, String fileNameOut,
			boolean overwrite) {
		return move(new File(fileNameIn), new File(fileNameOut), overwrite);
	}

	public static boolean move(File fileIn, File fileOut, boolean overwrite) {
		if (!fileIn.exists())
			return false;
		if (!fileIn.isFile())
			return false;
		if (fileOut.isDirectory())
			fileOut = new File((new StringBuilder()).append(fileOut.getPath())
					.append(File.separator).append(fileIn.getName()).toString());
		if (!overwrite) {
			if (fileOut.exists())
				return false;

		}
		if (!fileOut.exists())

			try {
				if (fileIn.getCanonicalFile()
						.equals(fileOut.getCanonicalFile()))
					return true;
			} catch (IOException ioex) {
				return false;
			}
		fileOut.delete();
		return fileIn.renameTo(fileOut);
	}

	public static boolean moveFile(String src, String dest) {
		return (new File(src)).renameTo(new File(dest));
	}

	public static boolean moveFile(File src, File dest) {
		return src.renameTo(dest);
	}

	public static boolean moveDir(String fileIn, String fileOut) {
		return moveDir(new File(fileIn), new File(fileOut));
	}

	public static boolean moveDir(File fileIn, File fileOut) {
		if (!fileIn.exists())
			return false;
		if (!fileIn.isDirectory())
			return false;
		if (!fileOut.exists())

			try {
				if (fileIn.getCanonicalFile()
						.equals(fileOut.getCanonicalFile()))
					return true;
			} catch (IOException ioex) {
				return false;
			}
		// return false;
		return fileIn.renameTo(fileOut);
	}

	public static boolean copyDir(String srcDir, String dstDir) {
		return copyDir(new File(srcDir), new File(dstDir));
	}

	public static boolean copyDir(File srcDir, File dstDir) {
		if (srcDir.isDirectory()) {
			if (!dstDir.exists())
				dstDir.mkdir();
			String files[] = srcDir.list();
			for (int i = 0; i < files.length; i++)
				if (!copyDir(new File(srcDir, files[i]), new File(dstDir,
						files[i])))
					return false;

			return true;
		} else {
			return copyFile(srcDir, dstDir);
		}
	}

	public static boolean delete(String fileName) {
		return delete(new File(fileName));
	}

	public static boolean delete(File fileIn) {
		return fileIn.delete();
	}

	public static boolean deleteDir(String pathName) {
		return deleteDir(new File(pathName));
	}

	public static boolean deleteDir(File path) {
		if (path.isDirectory()) {
			File files[] = path.listFiles();
			for (int i = 0; i < files.length; i++)
				if (!deleteDir(files[i]))
					return false;

		}
		return path.delete();
	}

	public static String readString(String fileName) throws IOException {
		return readString(new File(fileName), STRING_BUFFER_SIZE);
	}

	public static String readString(String fileName, int bufferSize)
			throws IOException {
		return readString(new File(fileName), bufferSize);
	}

	public static String readString(File file) throws IOException {
		return readString(file, STRING_BUFFER_SIZE);
	}

	public static String readString(File file, int bufferSize)
			throws IOException {
		long fileLen;
		FileReader fr;
		BufferedReader brin;
		char buf[];
		fileLen = file.length();
		if (fileLen <= 0L)
			if (file.exists())
				return "";
			else
				return null;
		if (fileLen > 0x7fffffffL)
			throw new IOException("File too big for loading into a String!");
		fr = null;
		brin = null;
		buf = null;
		fr = new FileReader(file);
		brin = new BufferedReader(fr, bufferSize);
		int length = (int) fileLen;
		buf = new char[length];
		brin.read(buf, 0, length);
		if (brin != null) {
			brin.close();
			fr = null;
		}
		if (fr != null)
			fr.close();

		if (brin != null) {
			brin.close();
			fr = null;
		}
		if (fr != null)
			fr.close();

		return new String(buf);
	}

	public static void writeString(String fileName, String s)
			throws IOException {
		writeString(new File(fileName), s, STRING_BUFFER_SIZE);
	}

	public static void writeString(String fileName, String s, int bufferSize)
			throws IOException {
		writeString(new File(fileName), s, bufferSize);
	}

	public static void writeString(File file, String s) throws IOException {
		writeString(file, s, STRING_BUFFER_SIZE);
	}

	public static void writeString(File file, String s, int bufferSize)
			throws IOException {
		FileWriter fw;
		BufferedWriter out;
		fw = null;
		out = null;
		if (s == null)
			return;
		fw = new FileWriter(file);
		out = new BufferedWriter(fw, bufferSize);
		out.write(s);
		if (out != null) {
			out.close();
			fw = null;
		}
		if (fw != null)
			fw.close();

		if (out != null) {
			out.close();
			fw = null;
		}
		if (fw != null)
			fw.close();

	}

	public static String readString(String fileName, String encoding)
			throws IOException {
		return readString(new File(fileName), STRING_BUFFER_SIZE, encoding);
	}

	public static String readString(String fileName, int bufferSize,
			String encoding) throws IOException {
		return readString(new File(fileName), bufferSize, encoding);
	}

	public static String readString(File file, String encoding)
			throws IOException {
		return readString(file, STRING_BUFFER_SIZE, encoding);
	}

	public static String readString(File file, int bufferSize, String encoding)
			throws IOException {
		FileInputStream fis;
		InputStreamReader isr;
		BufferedReader brin;
		int length;
		char buf[];
		int realSize;
		long fileLen = file.length();
		if (fileLen <= 0L)
			if (file.exists())
				return "";
			else
				return null;
		if (fileLen > 0x7fffffffL)
			throw new IOException("File too big for loading into a String!");
		fis = null;
		isr = null;
		brin = null;
		length = (int) fileLen;
		buf = null;
		realSize = 0;
		fis = new FileInputStream(file);
		isr = new InputStreamReader(fis, encoding);
		brin = new BufferedReader(isr, bufferSize);
		buf = new char[length];
		int c;
		while ((c = brin.read()) != -1) {
			buf[realSize] = (char) c;
			realSize++;
		}
		if (brin != null) {
			brin.close();
			isr = null;
			fis = null;
		}
		if (isr != null) {
			isr.close();
			fis = null;
		}
		if (fis != null)
			fis.close();

		if (brin != null) {
			brin.close();
			isr = null;
			fis = null;
		}
		if (isr != null) {
			isr.close();
			fis = null;
		}
		if (fis != null)
			fis.close();

		return new String(buf, 0, realSize);
	}

	public static void writeString(String fileName, String s, String encoding)
			throws IOException {
		writeString(new File(fileName), s, STRING_BUFFER_SIZE, encoding);
	}

	public static void writeString(String fileName, String s, int bufferSize,
			String encoding) throws IOException {
		writeString(new File(fileName), s, bufferSize, encoding);
	}

	public static void writeString(File file, String s, String encoding)
			throws IOException {
		writeString(file, s, STRING_BUFFER_SIZE, encoding);
	}

	public static void writeString(File file, String s, int bufferSize,
			String encoding) throws IOException {
		FileOutputStream fos;
		OutputStreamWriter osw;
		BufferedWriter out;
		if (s == null)
			return;
		fos = null;
		osw = null;
		out = null;
		fos = new FileOutputStream(file);
		osw = new OutputStreamWriter(fos, encoding);
		out = new BufferedWriter(osw, bufferSize);
		out.write(s);
		if (out != null) {
			out.close();
			osw = null;
			fos = null;
		}
		if (osw != null) {
			osw.close();
			fos = null;
		}
		if (fos != null)
			fos.close();

		if (out != null) {
			out.close();
			osw = null;
			fos = null;
		}
		if (osw != null) {
			osw.close();
			fos = null;
		}
		if (fos != null)
			fos.close();

	}

	public static void writeObject(String f, Object o) throws IOException {
		writeObject(f, o, OBJECT_BUFFER_SIZE);
	}

	public static void writeObject(String f, Object o, int bufferSize)
			throws IOException {
		FileOutputStream fos;
		BufferedOutputStream bos;
		ObjectOutputStream oos;
		fos = null;
		bos = null;
		oos = null;
		fos = new FileOutputStream(f);
		bos = new BufferedOutputStream(fos, bufferSize);
		oos = new ObjectOutputStream(bos);
		oos.writeObject(o);
		if (oos != null) {
			oos.close();
			bos = null;
			fos = null;
		}
		if (bos != null) {
			bos.close();
			fos = null;
		}
		if (fos != null)
			fos.close();

		if (oos != null) {
			oos.close();
			bos = null;
			fos = null;
		}
		if (bos != null) {
			bos.close();
			fos = null;
		}
		if (fos != null)
			fos.close();
	}

	public static Object readObject(String f) throws IOException,
			ClassNotFoundException, FileNotFoundException {
		return readObject(f, OBJECT_BUFFER_SIZE);
	}

	public static Object readObject(String f, int bufferSize)
			throws IOException, ClassNotFoundException, FileNotFoundException {
		Object result;
		FileInputStream fis;
		BufferedInputStream bis;
		ObjectInputStream ois;
		result = null;
		fis = null;
		bis = null;
		ois = null;
		fis = new FileInputStream(f);
		bis = new BufferedInputStream(fis, bufferSize);
		ois = new ObjectInputStream(bis);
		result = ois.readObject();
		if (ois != null) {
			ois.close();
			bis = null;
			fis = null;
		}
		if (bis != null) {
			bis.close();
			fis = null;
		}
		if (fis != null)
			fis.close();

		if (ois != null) {
			ois.close();
			bis = null;
			fis = null;
		}
		if (bis != null) {
			bis.close();
			fis = null;
		}
		if (fis != null)
			fis.close();

		return result;
	}

	public static final byte[] readBytes(String s) throws IOException {
		return readBytes(new File(s));
	}

	public static final byte[] readBytes(File file) throws IOException {
		FileInputStream fileinputstream = new FileInputStream(file);
		long l = file.length();
		if (l > 0x7fffffffL)
			throw new IOException("File too big for loading into a byte array!");
		byte byteArray[] = new byte[(int) l];
		int i = 0;
		for (int j = 0; i < byteArray.length
				&& (j = fileinputstream
						.read(byteArray, i, byteArray.length - i)) >= 0; i += j)
			;
		if (i < byteArray.length) {
			throw new IOException((new StringBuilder())
					.append("Could not completely read the file ")
					.append(file.getName()).toString());
		} else {
			fileinputstream.close();
			return byteArray;
		}
	}

	public static void writeBytes(String filename, byte source[])
			throws IOException {
		if (source == null) {
			return;
		} else {
			writeBytes(new File(filename), source, 0, source.length);
			return;
		}
	}

	public static void writeBytes(File file, byte source[]) throws IOException {
		if (source == null) {
			return;
		} else {
			writeBytes(file, source, 0, source.length);
			return;
		}
	}

	public static void writeBytes(String filename, byte source[], int offset,
			int len) throws IOException {
		writeBytes(new File(filename), source, offset, len);
	}

	public static void writeBytes(File file, byte source[], int offset, int len)
			throws IOException {
		FileOutputStream fos;
		if (len < 0)
			throw new IOException("File size is negative!");
		if (offset + len > source.length)
			len = source.length - offset;
		fos = null;
		fos = new FileOutputStream(file);
		fos.write(source, offset, len);
		if (fos != null)
			fos.close();

		if (fos != null)
			fos.close();

	}

	public static void copyPipe(InputStream in, OutputStream out,
			int bufSizeHint) throws IOException {
		int read = -1;
		byte buf[] = new byte[bufSizeHint];
		while ((read = in.read(buf, 0, bufSizeHint)) >= 0)
			out.write(buf, 0, read);
		out.flush();
	}

	public static File[] listFiles(File dir) {
		String ss[] = dir.list();
		if (ss == null)
			return null;
		int n = ss.length;
		File fs[] = new File[n];
		for (int i = 0; i < n; i++)
			fs[i] = new File(dir.getPath(), ss[i]);

		return fs;
	}

	public static File[] sortListFiles(File dir) {
		File fs[] = dir.listFiles();
		if (fs != null && fs.length > 1)
			Arrays.sort(fs, new Comparator() {

				public int compare(Object a, Object b) {
					File filea = (File) a;
					File fileb = (File) b;
					if (filea.isDirectory() && !fileb.isDirectory())
						return -1;
					if (!filea.isDirectory() && fileb.isDirectory())
						return 1;
					else
						return filea.getName().compareToIgnoreCase(
								fileb.getName());
				}

			});
		return fs;
	}

	public static File getFile(String fileName) {
		return new File(fileName);
	}

	public static boolean isDirectory(String path) {
		boolean dir = false;
		if (path != null) {
			File file = new File(path);
			dir = file.isDirectory();
		}
		return dir;
	}

	public static boolean isFile(String path) {
		boolean file = false;
		if (path != null) {
			File f = new File(path);
			file = f.isFile();
		}
		return file;
	}

	public boolean hasExtension(String exts[], String ext) {
		if (exts == null || ext == null)
			return false;
		if (exts.length == 0)
			return false;
		for (int i = 0; i < exts.length; i++)
			if (ext.equalsIgnoreCase(exts[i]))
				return true;

		return false;
	}

	public static String toDisplaySize(String file) {
		if (file == null || file.length() == 0)
			return "";
		else
			return toDisplaySize(new File(file));
	}

	public static String toDisplaySize(File file) {
		if (file == null || !file.exists())
			return "";
		else
			return toDisplaySize(file.length());
	}

	public static String toDisplaySize(long size) {
		return toDisplaySize((new Long(size)).intValue());
	}

	public static String toDisplaySize(int size) {
		String displaySize;
		if (size / 0x40000000 > 0)
			displaySize = (new StringBuilder())
					.append(String.valueOf(size / 0x40000000)).append(" GB")
					.toString();
		else if (size / 0x100000 > 0)
			displaySize = (new StringBuilder())
					.append(String.valueOf(size / 0x100000)).append(" MB")
					.toString();
		else if (size / 1024 > 0)
			displaySize = (new StringBuilder())
					.append(String.valueOf(size / 1024)).append(" KB")
					.toString();
		else
			displaySize = (new StringBuilder()).append(String.valueOf(size))
					.append(" bytes").toString();
		return displaySize;
	}

	public static boolean waitFor(String fileName, int seconds) {
		File file = new File(fileName);
		int timeout = 0;
		int tick = 0;
		do {
			if (file.exists())
				break;
			if (tick++ >= 10) {
				tick = 0;
				if (timeout++ > seconds)
					return false;
			}
			try {
				Thread.sleep(100L);
				continue;
			} catch (InterruptedException ignore) {
				continue;
			} catch (Exception ex) {
			}
			break;
		} while (true);
		return true;
	}

	public static String dirname(String filename) {
		int i = filename.lastIndexOf(File.separator);
		return i < 0 ? "" : filename.substring(0, i);
	}

	public static String filename(String filename) {
		int i = filename.lastIndexOf(File.separator);
		return i < 0 ? filename : filename.substring(i + 1);
	}

	public static String basename(String filename) {
		return basename(filename, extension(filename));
	}

	public static String basename(String filename, String suffix) {
		int i = filename.lastIndexOf(File.separator) + 1;
		int lastDot = suffix == null || suffix.length() <= 0 ? -1 : filename
				.lastIndexOf(suffix);
		if (lastDot >= 0)
			return filename.substring(i, lastDot - 1);
		if (i > 0)
			return filename.substring(i - 1);
		else
			return filename;
	}

	public static String extension(String filename) {
		int lastDot = filename.lastIndexOf('.');
		if (lastDot >= 0)
			return filename.substring(lastDot + 1);
		else
			return "";
	}

	public static String toFormatDate(long lastmodified, String format) {
		SimpleDateFormat df = null;
		if (format == null || format.length() == 0)
			df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		else
			df = new SimpleDateFormat(format);
		return df.format(new Date(lastmodified));
	}

	public static String getRenameDupleFile(File files[], String targetName) {
		String resultName = targetName;
		String baseName = PathUtil.baseName(targetName);
		String extName = PathUtil.extension(targetName);
		int startTag = baseName.lastIndexOf('[');
		int endTag = baseName.lastIndexOf(']');
		for (int i = 0; i < files.length; i++) {
			if (!files[i].isFile() || !targetName.equals(files[i].getName()))
				continue;
			if (startTag != -1 && endTag != -1) {
				String indexNo = baseName.substring(startTag + 1, endTag);
				String indexName = baseName.substring(0, startTag);
				int indexNoInt = 1;
				try {
					indexNoInt = Integer.parseInt(indexNo);
					indexNoInt++;
				} catch (NumberFormatException nfe) {
				}
				resultName = (new StringBuilder()).append(indexName)
						.append("[").append(indexNoInt).append("].")
						.append(extName).toString();
			} else {
				resultName = (new StringBuilder()).append(baseName)
						.append("[1].").append(extName).toString();
			}
			resultName = getRenameDupleFile(files, resultName);
		}

		return resultName;
	}

	public void makeBasePath(String path) {
		File dir = new File(path);
		if (!dir.exists())
			dir.mkdirs();
	}

	public String saveFile(MultipartFile multipartFile, String uploadPath) throws IOException {
        if (null == multipartFile || multipartFile.getName().equals("") || multipartFile.getSize() <= 0) return null;
        
        if(!(FileUtil.isDirectory(uploadPath))){
            FileUtil.mkdir(uploadPath);
       }
        
	    String orgname = multipartFile.getOriginalFilename();
 	    String fileName = orgname.substring(0, orgname.lastIndexOf("."));
 	    String exc = orgname.substring(orgname.lastIndexOf(".")+1, orgname.length());
 	      
 	    int i=0;
 	    //String third = String.format("%s%s",firstStr , secStr);
 	    String tempFileName = fileName;
	    do {
	    		if(!getFile(uploadPath+"/"+tempFileName + "." + exc).exists())
	    		{    		   
	    			fileName = tempFileName + "." + exc;
	    			break;
	    		}
	    		i++;
	    		tempFileName = fileName + "(" + i + ")";
	   } while (true);
	   
	   String filePath = uploadPath+"/"+fileName;
	   System.out.println("filePath : "+ filePath);
	       
       //write file
	   InputStream inputStream = multipartFile.getInputStream();
//       FileInputStream inputStream = (FileInputStream) multipartFile.getInputStream();
       FileOutputStream outputStream = new FileOutputStream(filePath);
       int bytesRead = 0;
       byte[] buffer = new byte[1024];
       while ((bytesRead = inputStream.read(buffer, 0, 1024)) != -1) {
            outputStream.write(buffer, 0, bytesRead);
       }
       outputStream.close();
       inputStream.close();
       
       //FileCopyUtils.copy(mpf.getBytes(), new FileOutputStream("D:/temp/files/"+mpf.getOriginalFilename()));
       return fileName;
       
    }

	public static boolean validUpload(String filetype) {
		String[] validExt = { "pdf", "png", "zip", "bmp", "exe", "gif", "htm",
				"hwp", "jpg", "ppt", "pptx", "tif", "txt", "xls", "xlx",
				"xlsx", "xml", "doc", "docx", "dat", "mac" };

		if (filetype == null)
			return false;

		for (int i = 0; i < validExt.length; i++) {
			if (filetype.toLowerCase().equals(validExt[i])) {
				return true;
			}
		}
		return false;
	}

	/**
	 * �̹��� ��� ��
	 * 
	 * @param rootPath
	 * @return
	 */
	public static String getImagePath(String rootPath) {

		String sYear = DateUtil.getYYYY();
		String sMonthDay = DateUtil.getMM();
		StringBuffer sbImagePath = new StringBuffer();

		File filePath = null;
		filePath = new File(rootPath + sbImagePath.toString());
		if (!filePath.exists())
			filePath.mkdir();

		sbImagePath.append("/" + sYear);
		filePath = new File(rootPath + sbImagePath.toString());
		if (!filePath.exists())
			filePath.mkdir();

		sbImagePath.append("/" + sMonthDay);
		filePath = new File(rootPath + sbImagePath.toString());
		if (!filePath.exists())
			filePath.mkdir();

		return sbImagePath.toString();
	}

	/**
	 * 
	 * @param fileName
	 * @return
	 */
	public static String getFileName(String fileName) {
		System.out.println("fileName:" + fileName);
		int posExt = fileName.lastIndexOf(".");
		long serial = -1;
		String ext = "";
		if (posExt > 0) {
			ext = fileName.substring(posExt + 1);
		}
		if (serial < 0) {
			serial = System.currentTimeMillis();
		}
		serial++;
		String tmp = Long.toHexString(serial) + "." + ext;
		return tmp;
	}

	public static int FILE_BUFFER_SIZE = 32768;
	public static int STRING_BUFFER_SIZE = 32768;
	public static int OBJECT_BUFFER_SIZE = 32768;
	public static final int ONE_KB = 1024;
	public static final int ONE_MB = 0x100000;
	public static final int ONE_GB = 0x40000000;
	
	private static List<String> imageExtensions = new ArrayList<String>();

    static {
        imageExtensions.addAll(Arrays.asList(new String[]{"gif", "jpg", "jpeg", "png","bmp"}));
    }

    /**
     * 파일을 업로드 한다.
     * <xmp>
     * 예)
     *  MultipartHttpServletRequest multipartReq = (MultipartHttpServletRequest)req;
	 *	Map<String, MultipartFile> files = multipartReq.getFileMap();
     *
     *  List<MultipartFile> multiFiles = new ArrayList<MultipartFile>();
     *  List<String> saveFileNames = new ArrayList<String>();
     *
     *  String fileName = null;
     *  for (MultipartFile file : files.values()) {
     *       multiFiles.add(file);
     *       fileName = file.getOriginalFilename();
     *       saveFileNames.add(FileManagement.changeSaveFileName(fileName));
     *   }
     *   FileManagement.uploadFile(req,"/imageFile",multiFiles,saveFileNames);
     * </xmp>
     * @param req
     * @param saveFileLocation  저장될 위치 (image인경우는 cntext_root/saveFileLocation 에 저장된다.)
     * @param multiFiles MultipartFile 리스트
     * @param saveFileNames file이름들의 리스트
     * @throws Exception
     */
    public static void uploadFile(HttpServletRequest req, String saveFileLocation, List<MultipartFile> multiFiles, List<String> saveFileNames) throws Exception {
        String applicationRealPath = applicationRealPath = req.getSession().getServletContext().getRealPath("");
        //String webInfoPath = webInfoPath = applicationRealPath + "/WEB-INF";
        File dir = new File(saveFileLocation);
        if (!dir.exists()) {
            dir.mkdir();
        }

        for (int i = 0; i < multiFiles.size(); i++) {
            if(checkImageFile(multiFiles.get(i).getOriginalFilename())){   //image인경우
                dir = new File(applicationRealPath+saveFileLocation);
                if (!dir.exists()) {
                    dir.mkdir();
                }
                FileCopyUtils.copy(multiFiles.get(i).getInputStream(), new FileOutputStream(applicationRealPath+saveFileLocation + "/" + saveFileNames.get(i)));
            }else{
                FileCopyUtils.copy(multiFiles.get(i).getInputStream(), new FileOutputStream(saveFileLocation + "/" + saveFileNames.get(i)));
            }
        }
    }

    /**
     * 파일을 다운로드 한다.
     * <xmp>
     * 예)
	 * FileManagement.downloadFile(request,response,saveFileLocation,request.getParameter("originalFileName"),request.getParameter("saveFileName"));
     * client
     * <img src="http://localhost:8080/path/saveFileName/> 또는
     * <a href="/download.do?originalFileName=originalFileName&saveFileName=saveFileName/>"
     * </xmp>
     * @param req
     * @param res
     * @param path 다운로드할 위치 (image인경우는 cntext_root/saveFileLocation 에 다운로드 된다.)
     * @param originalFileName 다운로드할 파일명
     * @param saveFileName 실제 서버에 저장된 파일명
     * @throws Exception
     */
	public static void downloadFile(HttpServletRequest req, HttpServletResponse res, String path, String originalFileName, String saveFileName)throws Exception{
		/** Build File Full Path */
		StringBuffer fileFullPath = new StringBuffer();
        if(checkImageFile(saveFileName)){   //image인경우
            String imageDownloadPath = req.getSession().getServletContext().getRealPath("")+path;
            fileFullPath.append(imageDownloadPath).append(File.separatorChar).append(saveFileName);
        }else{
            fileFullPath.append(path).append(File.separatorChar).append(saveFileName);
        }

		File dwldFile = new File(fileFullPath.toString());

		/** Set Response Header */
		res.setHeader("Content-Type", "application/octet-stream; charset=UTF-8");
		res.setContentLength((int)dwldFile.length());
		res.setHeader("Content-Disposition","attachment; fileName="+"\""+new String(originalFileName.getBytes("MS949"), "8859_1")+"\""+";");
		res.setHeader("Content-Transfer-Encoding","binary");
		OutputStream os = res.getOutputStream();
		InputStream is = null;
		is = new FileInputStream(dwldFile);
		FileCopyUtils.copy(is,os);
		os.flush();
	}

    /**
     * 저장될 파일명의 이름을 변경해준다.
     * <xmp>
     * 예)dd.jpg  -> dd_1121245206883.jpg  (nano seconds)
     * </xmp>
     * @param fileName
     * @return
     */
    public static String changeSaveFileName(String fileName){
        String[] fileNameSplit = fileName.split("\\.");
        fileNameSplit[fileNameSplit.length-2] = fileNameSplit[fileNameSplit.length-2]+"_"+System.nanoTime();
        StringBuilder sb = new StringBuilder();
        for(String split : fileNameSplit){
            sb.append(split+".");
        }

        return sb.toString().substring(0,sb.toString().length()-1);
    }

    /**
     * 이미지 파일인지를 체크한다.
     *
     * @param file
     * @return
     */
    public static boolean checkImageFile(String file) {
        String[] fileSplit = file.split("\\.");
        if (imageExtensions.contains(fileSplit[fileSplit.length - 1])) {
            return true;
        }
        return false;
    }
    
    public static String createUUID() {
    	  
	  return UUID.randomUUID().toString();
	  
	 }
    
    public static String makeFileHashcode(byte[] t_byte_arr) throws Exception {
		String algorythm = "SHA-1";
		MessageDigest md = MessageDigest.getInstance(algorythm);
		md.update(t_byte_arr);

		byte[] hash_arr = md.digest();

		String hashToString = "";
		for (int i = 0; i < hash_arr.length; i++) {
			hashToString += Integer.toString((hash_arr[i] & 0xff) + 0x100, 16)
					.substring(1);
		}

		return hashToString.toUpperCase();
	}

	public static java.util.Date getCurrentDate() {
		Calendar now = Calendar.getInstance();
		return now.getTime();
	}
	

}
