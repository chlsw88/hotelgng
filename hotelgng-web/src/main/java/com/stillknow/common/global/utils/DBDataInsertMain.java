package com.stillknow.common.global.utils;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

//import com.cepo.adaptor.database.util.SqlMapperUtil;

public class DBDataInsertMain {
	
	private static DmlType allEffectDmlType = null;
	
	public void handle(Connection con, File xlsPath) throws Exception {
		class JXLReaderHandler implements IJXLReaderHander
		{
			private Connection con;
			private Map<Integer, PreparedStatement> statementMap; 
			
			public JXLReaderHandler(Connection con)
			{
				this.con = con;
				statementMap = new HashMap<Integer, PreparedStatement>();
			}
			
			@Override
			public boolean isResultMark() {
				return false;
			}

			@Override
			public boolean rowReaded(TableInfo tableInfo, DmlType dmlType, Map<String, String> dataMap) throws Exception {
				
				// ������ ���ǵ� I/U/D�� ������ ���
				if(allEffectDmlType!=null) {
					dmlType = allEffectDmlType;
				}
				
				if(dmlType==DmlType.UNKNOWN) {
//					System.err.println("SKIP DATA : "+dataMap);
					return false;
				}
				System.out.println(dmlType+" DATA : "+dataMap);
				
				PreparedStatement ps = statementMap.get(tableInfo.getSql(dmlType).hashCode());
				if(ps == null) {
					ps = con.prepareStatement(tableInfo.getSql(dmlType));
					statementMap.put(tableInfo.getSql(dmlType).hashCode(), ps);
				}
				
				if(dmlType == DmlType.INSERT) {
					// DATA
					for(int colIndex=0;colIndex<tableInfo.columnList.size();colIndex++)
					{
						String data = dataMap.get(tableInfo.columnList.get(colIndex));
						if(data.equals("$CURRENTDATE")) {
							ps.setDate(colIndex+1, getSqlCurrentDate());
						}
						else {
							ps.setString(colIndex+1, data);
						}
					}
				} else if(dmlType == DmlType.UPDATE) {
					// DATA
					for(int colIndex=0;colIndex<tableInfo.columnList.size();colIndex++)
					{
						String data = dataMap.get(tableInfo.columnList.get(colIndex));
						if(data.equals("$CURRENTDATE")) {
							ps.setDate(colIndex+1, getSqlCurrentDate());
						}
						else {
							ps.setString(colIndex+1, data);
						}
					}
					// WHERE
					for(int colIndex=0;colIndex<tableInfo.pkList.size();colIndex++)
					{
						ps.setString(colIndex+1+tableInfo.columnList.size(), dataMap.get(tableInfo.columnList.get(colIndex)));
					}
				} else if(dmlType == DmlType.DELETE) {
					// WHERE
					for(int colIndex=0;colIndex<tableInfo.pkList.size();colIndex++)
					{
						ps.setString(colIndex+1, dataMap.get(tableInfo.pkList.get(colIndex)));
					}
				}
				
				int updateCount = ps.executeUpdate();
				return updateCount == 1;
			}
			
			private java.sql.Date getSqlCurrentDate() {
				Calendar cal = Calendar.getInstance();
				java.util.Date uDate = cal.getTime();
				java.sql.Date sDate = new java.sql.Date(uDate.getTime());
				return sDate;
			}
		}
		
		JXLReaderHandler handler = new JXLReaderHandler(con);
		
		//JXLReader r = new JXLReader(xlsPath);
		
		con.setAutoCommit(false);
		
		try {
			//r.read(handler);
			con.commit();
		} catch(Exception e) {
			con.rollback();
		}
	}
	
	public static void main(String[] args) throws Exception {
		
//		Class.forName("org.mariadb.jdbc.Driver");
//		Connection con = DriverManager.getConnection("jdbc:mysql://1.214.91.41.:3306/cepo", "root", "!cepo1234");

		//SqlMapperUtil.initialize();
		
//		String dataPath = "D:/EclipseWorkspace/CEPOServerSingle/src-manage/com/cepo/db/data";
		String dataPath = "D:/Dropbox/stillknow/dev/workspace/stillknow/src/main/java/com/stillknow/common/global/utils";
		
//		allEffectDmlType = DmlType.INSERT; 
		
		//Connection con = SqlMapperUtil.getSession().getConnection();
		//System.out.println(con);
		
		DBDataInsertMain jxl = new DBDataInsertMain();
//		File xlsPath1 = new File(dataPath+"/quest2.xls");
//		jxl.handle(con, xlsPath1);
//		File xlsPath2 = new File(dataPath+"/quiz.xls");
//		jxl.handle(con, xlsPath2);
//		File xlsPath3 = new File(dataPath+"/res.xls");
//		jxl.handle(con, xlsPath3);
//		File xlsPath4 = new File(dataPath+"/code.xls");
//		jxl.handle(con, xlsPath4);
		File xlsPath5 = new File(dataPath+"/quest.xls");
		//jxl.handle(con, xlsPath5);
		
		//con.close();
	}
}
