package com.stillknow.common.global.utils;

public enum DmlType {

	INSERT,
	
	UPDATE,
	
	DELETE,
	
	UNKNOWN
}
