package com.stillknow.common.global.utils;

import java.io.FileInputStream;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.mongodb.DB;
import com.mongodb.DBAddress;
import com.mongodb.DBCollection;
import com.stillknow.common.domain.FileVo;
//import java.nio.file.Files;
//import java.nio.file.Path;
//import java.nio.file.Paths;
import com.stillknow.common.domain.MngFile;

public class ImageCommon {
	
	private static final Logger logger = LoggerFactory.getLogger(ImageCommon.class);

	public int save(MngFile file, String serverIp, String userId) throws Exception {
		
		int rnt = 0;
		try {
			
			String[] serverInfos = serverIp.split("/");
			String[] ipInfos = serverInfos[0].split(":");
			
			String serverIP = ipInfos[0];	//163.180.56.25
			int port = Integer.parseInt(ipInfos[1]);
			String dbName = serverInfos[1];	//BINARYDATA_DB
			String imageDataColumnName = serverInfos[2];	//COL_IMAGEDATA
						
			DB db = com.mongodb.Mongo.connect(new DBAddress(serverIP, port, dbName));
			DBCollection imageDBCollection = db.getCollection(imageDataColumnName);;
	
			// 저장하는 주체
			//String userId = "WebAdmin";
			// 파일저장시 이름은 룰을 정할것 (예:CompanyId_imageName_001)
			// 이미지는 회사별로 디렉토리 만들어서 이미지이름_순번 으로 하면 좋을듯 함)
			// 이미지는 jpg 또는 png로 할것. (앱 호환 이미지)
			String imageUniqueName = file.getfileid();
			// 이미지 데이터 로드
			//Path pathR = Paths.get(file.getFilePath());
			//byte[] pictureDataBin = Files.readAllBytes(pathR);
	
			InputStream stream = new FileInputStream(file.getuploadpath());;
			byte[] data = org.apache.commons.io.IOUtils.toByteArray(stream);
			
			// 이미지 데이터 저장
			// 이미지 저장은 내부에서 파일 imageUniqueName와 해쉬코드로 쌍으로 저장되며
			// 이미지 내용 변경시 앱에서 자동 갱신 됨.
			IImageDataManageLib manage = new ImageDataManageLib();
			manage.saveImage(imageDBCollection, userId, imageUniqueName, data);
			rnt = 1;
		} catch( Exception e ) {
			logger.debug("image save result ::: " +e.getMessage()+"\n");
		}
		return rnt;
	}
}

