package com.stillknow.common.global.utils;

import java.security.MessageDigest;
import java.util.Calendar;
import java.util.Iterator;

import com.google.protobuf.ByteString;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;


public class ImageDataManageLib implements IImageDataManageLib {

	@Override
	public void saveImage(DBCollection imageDBCollection, String userId, String imageName, byte[] pictureDataBin) throws Exception {
		
		if(pictureDataBin==null) {
			System.out.println(imageName+"  bytes Data is empty!!!");
			return;
		}
		
		// �̹��� Ű�� �����̸��� ��ҹ��ڸ� �������� �ʱ� ���ؼ� �ҹ��ڷ� �Ѵ�.
		imageName = imageName.toLowerCase();
		
		String imageHashcode = makeFileHashcode(pictureDataBin);

		DBObject objSearch = new BasicDBObject();
		objSearch.put(ImageDataKey.USERID.name(), userId);
		objSearch.put(ImageDataKey.IMAGENAME.name(), imageName);

		BasicDBObject updateObject = new BasicDBObject();
		updateObject.put("$set", new BasicDBObject().append(ImageDataKey.IMAGEHASHCODE.name(), imageHashcode).
													 append(ImageDataKey.IMAGEDATABYTE.name(), pictureDataBin).
													 append(ImageDataKey.CREATEDATE.name(), getCurrentDate()));

		WriteResult result = imageDBCollection.update(objSearch, updateObject);
		if(result.getN()==0) {
			DBObject insertObject = new BasicDBObject();
			insertObject.put(ImageDataKey.USERID.name(), userId);
			insertObject.put(ImageDataKey.IMAGETYPE.name(), ImageType.C.name());

			insertObject.put(ImageDataKey.IMAGENAME.name(), imageName);
			insertObject.put(ImageDataKey.IMAGEHASHCODE.name(), imageHashcode);
			insertObject.put(ImageDataKey.IMAGEDATABYTE.name(), pictureDataBin);
			insertObject.put(ImageDataKey.CREATEDATE.name(), getCurrentDate()); 
			imageDBCollection.save(insertObject);
		}
	}

	@Override
	public byte[] getImage(DBCollection imageDBCollection, String imageHashcode) throws Exception {
		DBCursor cur = null;
		try
		{
			BasicDBObject query = new BasicDBObject();
			query.put(ImageDataKey.IMAGEHASHCODE.name(), imageHashcode);
			cur = imageDBCollection.find(query).limit(1);
			
			Iterator<DBObject> it = cur.iterator();
			if (it.hasNext()) {
				DBObject obj = it.next();
				byte[] pData = (byte[])obj.get(ImageDataKey.IMAGEDATABYTE.name());
				if(pData!=null) {
					return pData;
				}
			}
		}
		finally
		{
			if ( null != cur ) cur.close();
		}
		
		return null;
	}
	
//	@Override
//	public ByteString getImage(DBCollection imageDBCollection, String imageHashcode) throws Exception {
//		DBCursor cur = null;
//		try
//		{
//			BasicDBObject query = new BasicDBObject();
//			query.put(ImageDataKey.IMAGEHASHCODE.name(), imageHashcode);
//			cur = imageDBCollection.find(query).limit(1);	
//			
//			Iterator<DBObject> it = cur.iterator();
//			if (it.hasNext()) {
//				DBObject obj = it.next();
//				byte[] pData = (byte[])obj.get(ImageDataKey.IMAGEDATABYTE.name());
//				if(pData!=null) {
//					return ByteString.copyFrom(pData);
//				}
//			}
//		}
//		finally
//		{
//			if ( null != cur ) cur.close();
//		}
//		
//		return null;
//	}
	
//	@Override
//	public String getImage(DBCollection imageDBCollection, String imageHashcode) throws Exception {
//		DBCursor cur = null;
//		try
//		{
//			BasicDBObject query = new BasicDBObject();
//			query.put(ImageDataKey.IMAGEHASHCODE.name(), imageHashcode);
//			cur = imageDBCollection.find(query).limit(1);	
//			
//			Iterator<DBObject> it = cur.iterator();
//			if (it.hasNext()) {
//				DBObject obj = it.next();
//				String pData = (String) obj.get(ImageDataKey.IMAGENAME.name());
//				if(pData!=null) {
//					return pData;
//				}
//			}
//		}
//		finally
//		{
//			if ( null != cur ) cur.close();
//		}
//		
//		return null;
//	}

	public void removeImage(DBCollection imageDBCollection, String imageHashcode) throws Exception {
		// �̹��� Ű�� �����̸��� ��ҹ��ڸ� �������� �ʱ� ���ؼ� �ҹ��ڷ� �Ѵ�.
		imageHashcode = imageHashcode.toLowerCase();

		BasicDBObject searchQuery = new BasicDBObject().
				append(ImageDataKey.IMAGEHASHCODE.name(), imageHashcode);
		imageDBCollection.remove(searchQuery);
	}
	public String makeFileHashcode(byte[] t_byte_arr) throws Exception {
		String algorythm = "SHA-1";
		MessageDigest md = MessageDigest.getInstance(algorythm);
		md.update(t_byte_arr);

		byte[] hash_arr = md.digest();

		String hashToString = "";
		for (int i = 0; i < hash_arr.length; i++) {
			hashToString += Integer.toString((hash_arr[i] & 0xff) + 0x100, 16)
					.substring(1);
		}

		return hashToString.toUpperCase();
	}

	public java.util.Date getCurrentDate() {
		Calendar now = Calendar.getInstance();
		return now.getTime();
	}
}
