package com.stillknow.common.global.utils;

import javax.xml.parsers.ParserConfigurationException;

import com.stillknow.common.global.component.ParseDOM;

public class XmlParser {
	
	public void parseDOM () throws ParserConfigurationException{
		  ParseDOM parseDom = new ParseDOM();
		  String xmlName = this.getClass().getResource("sample.xml").getPath();
		  parseDom.parseDOM(xmlName);
		 } 
}
