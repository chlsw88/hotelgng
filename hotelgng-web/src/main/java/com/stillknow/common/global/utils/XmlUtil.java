package com.stillknow.common.global.utils;

import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;


public class XmlUtil {
	
	public static final String XML_CONTENT_TYPE = "text/xml;charset=UTF-8";
	 

	public static Document getDocument(){
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		Document doc = null;
		try {
			builder = factory.newDocumentBuilder();
			doc = builder.newDocument();
		} catch (ParserConfigurationException e) {
			System.out.println("Error : XmlUtil.getDocument");
			e.printStackTrace();
		} finally {
		}
		return doc;
	}
	

	public static Document getDocument(File file) throws IOException {
		Document doc = null;
		
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF-8"));;
		try{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = null;
			builder = factory.newDocumentBuilder();
			InputSource inputSource = new InputSource(br);
			doc = builder.parse(inputSource);
		}catch(Exception e){
			System.out.println(e.toString());
		}finally{
			br.close();
		    br = null;
		}
		return doc;
	}

	public static Document getDocument(String str){
		Document doc = null;
		try{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = null;
			builder = factory.newDocumentBuilder();
			StringReader userdataReader = new StringReader(str);
			InputSource inputSource = new InputSource(userdataReader);
			doc = builder.parse(inputSource);
		}catch(Exception e){
			System.out.println(e.toString());
		}
		return doc;
	}
	

	public static String getDocumentAsXml(Document doc) {
		StringWriter sw = new java.io.StringWriter();
		try {
			DOMSource domSource = new DOMSource(doc);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.ENCODING,"UTF-8");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			StreamResult sr = new StreamResult(sw);
			transformer.transform(domSource, sr);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sw.toString();
	}
	

	public static Document toXMLDocument(List list) {
		
		Document doc = getDocument();		
		Element elem = doc.createElement("ITEMS");
		doc.appendChild(elem);		
		objectToXML(list, doc, elem);		
		return doc;
	}
	

	public static Document toXMLDocumentByDataMap(List<DataMap> list) {
		
		Document doc = getDocument();		
		Element elem = doc.createElement("ITEMS");
		doc.appendChild(elem);		
		dataMapToXML(list, doc, elem);		
		return doc;
	}
	

	public static Document toXMLDocument(Object pojo) {
		
		Document doc = getDocument();
		
		Element elem = doc.createElement("ITEM");
		doc.appendChild(elem);
		
		if(pojo != null)
			objectToXML(pojo, doc, elem);
		
		return doc;
	}
	

	public static Document toXMLDocumentByDataMap(DataMap dataMap) {
		
		Document doc = getDocument();
		
		Element elem = doc.createElement("ITEM");
		doc.appendChild(elem);
		
		if(dataMap != null)
			dataMap.toXMLDocument(elem);
		
		return doc;
	}
	

	public static void objectToXML(List list, Document doc, Element elem) {
		
		if(list == null){
			elem.setAttribute("length", "0");
			return;
		}
		
		int listSize = list.size();
		elem.setAttribute("length", listSize+"");
		
		for(int i = 0; i < listSize; i++){
			
			if(		list.get(i).getClass() == ArrayList.class 
				|| 	list.get(i).getClass() == List.class){	//List ?�태�??��??�수 ?�출
				
				List subList = (List)list.get(i);
				Element listElem = doc.createElement("ITEMS");
				elem.appendChild(listElem);
				objectToXML(subList, doc, listElem);	
			
			}else{											//POJO ?�태
				Object pojo = list.get(i);
				Element row = doc.createElement("ITEM");
				elem.appendChild(row);
				objectToXML(pojo, doc, row);			
			}
		}
	}
	

	public static void dataMapToXML(List<DataMap> list, Document doc, Element elem) {
		
		if(list == null){
			elem.setAttribute("length", "0");
			return;
		}
		
		int listSize = list.size();
		elem.setAttribute("length", listSize+"");
		
		for(int i = 0; i < listSize; i++){
			
														//POJO ?�태
			DataMap dataMap = list.get(i);
			Element row = doc.createElement("ITEM");
			elem.appendChild(row);
			dataMap.toXMLDocument(row);
		}
	}
	
	public static DataMap xmlToDataMap(Node node) {
		
		DataMap dataMap = new DataMap();
		
		NamedNodeMap nodeMap = node.getAttributes();
		for(int i = 0; i < nodeMap.getLength(); i++){
			Node item = nodeMap.item(i);
			String key = item.getNodeName();
			String value = item.getNodeValue();
			dataMap.put(key, value);
		}
		return dataMap;
	}

	public static void objectToXML(Object pojo, Document doc, Element elem){
		if(pojo == null)
			return;
		
		try {
			PropertyDescriptor[] props = Introspector.getBeanInfo(pojo.getClass()).getPropertyDescriptors();
		
			for (int i=0; i < props.length; i++) {
				
				PropertyDescriptor prop = props[i];
				
				String name = prop.getName();
				
				if (name.equals("class")) continue;
				Method getter = prop.getReadMethod();
				if(getter != null){
				
					if(		prop.getPropertyType() == List.class
						|| 	prop.getPropertyType() == ArrayList.class){	//List ?�태�??�식?�로
						
						List subList = (List)getter.invoke(pojo, null);
						//?�게 Null ?�면 ?�그?�체�?만들�?말까 ?�쩔�?.
						//밑에 Attribute??같�? ?�리�??�자.
						Element listElem = doc.createElement( name );
						elem.appendChild(listElem);
						objectToXML(subList, doc, listElem);
					
					}else{												//POJO ?�태�??�트리뷰?�로
						
						Object objVal = getter.invoke(pojo, null);
						String val = "";
						if (objVal == null)	val = "";
						else				val = objVal.toString();
						
						elem.setAttribute(name, val);
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	public static void overrideXmlList(
										NodeList target, String targetTag, 
										NodeList parent, String parentTag){
		
		for( int i=0; i<target.getLength(); i++ ){
			Element targetColumn = (Element)target.item(i);
			Element parentColumn = null;
			
			String targetTagValue = targetColumn.getAttribute(targetTag);
			for( int j=0; j<parent.getLength(); j++ ){
				
				if( targetTagValue.equals(((Element)parent.item(j)).getAttribute(parentTag)) ){
					parentColumn = (Element)parent.item(j);
					break;
				}
			}
			
			if( parentColumn != null ){
				overrideXml(targetColumn, parentColumn);
			}
		}
	}
	

	public static Element overrideXml(Element target, Element parent){
		
		if(parent != null){
			
			NamedNodeMap namedNodeMap = parent.getAttributes();
			for( int i=0; i<namedNodeMap.getLength(); i++){
				
				Node attributeNode = namedNodeMap.item(i);
				String parentAttributeName = attributeNode.getNodeName();
				String parentAttributeValue = attributeNode.getNodeValue();
				
				// attribute override
				if(!target.hasAttribute(parentAttributeName)){
					target.setAttribute(parentAttributeName, parentAttributeValue);
				}
				
				// children override
				if(parent.getChildNodes().getLength() > 0){
					if(target.getChildNodes().getLength() == 0){
						for (int j=0; j<target.getChildNodes().getLength(); j++){
							
							target.appendChild(target.getChildNodes().item(j));
						}
					}
				}
				
			}
		}
		
		return target;
	}
	
	/**
	 * @param item
	 */
	public static void printNodeInfo(Node node) {
		
		String value = "child node";
		short childType = getChildType(node);
		
		if (childType == Node.TEXT_NODE) {
			value = node.getTextContent();
		}
		
		StringBuilder sb = new StringBuilder(200);
		sb.append("node name:").append(node.getNodeName())
				.append(", node value:").append(value);
		
		if (node.hasAttributes()) {
			NamedNodeMap attributeMap = node.getAttributes();
			int attributeLength = attributeMap.getLength();
			for (int i = 0; i < attributeLength; i++) {
				Node attNode = attributeMap.item(i);
				sb.append('\n').append("attr name:")
						.append(attNode.getNodeName()).append(", attr value:")
						.append(attNode.getNodeValue());
			}
		}
		
		System.out.println(sb);
		
		if (node.hasChildNodes()) {
			NodeList nodeList = node.getChildNodes();
			int nodeLength = nodeList.getLength();
			
			for (int i = 0; i < nodeLength; i++) {
				Node childNode = nodeList.item(i);
				
				if (childNode.getNodeType() == Node.TEXT_NODE) {
					continue;
				}
				printNodeInfo(nodeList.item(i));
			}
		}
		
	}

	/**
	 * @param node
	 * @return
	 */
	private static short getChildType(Node node) {
		
		NodeList nodeList = node.getChildNodes();
		int length = nodeList.getLength();
		for (int i = 0; i < length; i++) {
			Node childNode = nodeList.item(i);
			if (childNode.getNodeType() != Node.TEXT_NODE)
				return childNode.getNodeType();
		}
		return Node.TEXT_NODE;
	}

	/** 2014.03.08 SubongKim
		nodeList를 map 형태로 변환하여 반환. 자식노드가 또 다른 nodeList를 가지는 경우 재귀함수 호출.
		ex) <response>1</response><code>2</code> ==> map = {"response":"1","code":"2"}
		부모키값 구분자 "__" parent__child__grandChild의 형태로 key값으로 들어간다.
		자식 노드의 값이 node인경우 child node가 value 값으로 삽입된다.
	 *
	 * */
	public static Map getNodeMap(NodeList nodeList, String parentNode) {
		// 		
		String value = "child node";
		Map nodeMap = new HashMap<String, Object>();
		parentNode = "".equals(parentNode) ? "" : parentNode+"__" ;
		Node node;
		for(int i=0; i<nodeList.getLength();i++){
			node = nodeList.item(i);
			short childType = getChildType(node);
			
			StringBuilder sb = new StringBuilder(200);
			if (!"#text".equals(node.getNodeName()) && childType == Node.TEXT_NODE) {
				value = node.getTextContent();
			}
			
			if(!"#text".equals(node.getNodeName())){
				nodeMap.put(parentNode+node.getNodeName(), value);
				sb.append("node name:").append(node.getNodeName())
				.append(", node value:").append(value);
			}
			
			
			/*
			if (node.hasAttributes()) {
				NamedNodeMap attributeMap = node.getAttributes();
				int attributeLength = attributeMap.getLength();
				for (int j = 0; j < attributeLength; j++) {
					Node attNode = attributeMap.item(j);
					sb.append('\n').append("attr name:")
							.append(attNode.getNodeName()).append(", attr value:")
							.append(attNode.getNodeValue());
				}
			}
			*/
			
			System.out.println(sb);
			
			if (node.hasChildNodes()) {
				nodeMap.putAll( getNodeMap( node.getChildNodes(), parentNode+node.getNodeName() ) );
			}
		}
		return nodeMap;
	}
	
	public static Map getMapToUrlConn(String addr) {
		StringBuffer strBuffer = new StringBuffer();
		
		try {
			URL url = new URL(addr);
			URLConnection urlconn = url.openConnection();
			urlconn.setRequestProperty("Content-Type", "application/x-www-form-utf-8");
			InputStreamReader in = new InputStreamReader(urlconn.getInputStream(),"UTF-8");
			int ch = 0;
			// 이부분에서 읽어들인 페이지의 텍스트를 파일로 저장하면 됩니다
			while (true) {
				try {
					ch = in.read();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				if (ch == -1)
					break;
				strBuffer.append((char)ch);
				//System.out.write((char)ch);
			}// while
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Document document = getDocument(strBuffer.toString());
		if( document == null || !document.hasChildNodes() )
			return null;	
		
		NodeList nodeList = document.getChildNodes();
		Map resultMap = getNodeMap(nodeList,"");

		//result
		return resultMap;
	}
	
	
}
