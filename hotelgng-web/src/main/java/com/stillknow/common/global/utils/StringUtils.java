package com.stillknow.common.global.utils;

import java.math.BigDecimal;
import java.util.Iterator;

/**
 * @author		Bong
 * @category	Utill
 * @version 	1.0
 * @since		2014-03-24
 */
public class StringUtils {
	
	
	public static boolean isEmpty(Object str) {
		return (str == null || "".equals(str));
	}
	
	  /**
     * 입력된 숫자에 대한 반올림.
     * @param value 입력된 숫자
     * @param scale 최종 소숫점 자리('3' 입력시 4째 자리에서 반올림)
     * @return Double형의 반올림된 결과 
     * [사용예]
     * Double rNum = Util.roundhalfup(1235.678756, 3);
     * System.out.println(rNum);  //1235.679 반환
     */
	public static Double roundhalfup(Double value, int scale) {
		if (value == null)
			return null;
		BigDecimal b = new BigDecimal(String.valueOf(value));
		return new Double(b.setScale(scale, BigDecimal.ROUND_HALF_UP).toString());
	}
	
    /**
      * 입력된 숫자에 대한 내림
      * @param value 입력된 숫자
      * @param scale 최종 소숫점 자리('3' 입력시 3째 자리까지의 숫자만 반환)
      * @return  Double형의 내림된 결과 
      *    [사용예]
      *     rNum =  Util.rounddown(1235.678856, 3);
      *     System.out.println(rNum); //1235.678 반환
     */
	public static Double rounddown(Double value, int scale) {
		if (value == null)
			return null;
		BigDecimal b = new BigDecimal(String.valueOf(value));
		return new Double(b.setScale(scale, BigDecimal.ROUND_DOWN).toString());
	}

    /**
      * 특정 자리수에서 무조건 반올림하기 
      * @param value 입력된 숫자
      * @param scale 최종 소숫점 자리('3' 입력시 3째 자리 숫자가 반올림된 값으로 반환)
      * @return Double형의 올림된 결과 
      *   [사용예]
      *    rNum = Util.rounddown(1235.678256, 3);
      *    System.out.println(rNum); //1235.679 반환
      */
	public static Double roundup(Double value, int scale) {
		if (value == null)
			return null;
		BigDecimal b = new BigDecimal(String.valueOf(value));
		return new Double(b.setScale(scale, BigDecimal.ROUND_UP).toString());
	}
	
	
	/**
     * 하나의 문자열(target)을 특정 구분자(cutStr)를 기준으로 두개의 문자열로 분리한다.
     * @param target 입력된 문자열
     * @param cutStr 구분자
     * @return String[] 결과 문자열 배열
     *   [사용예]
     *     resultArray = Util.divide("aaaa11111 /bbbcccc", "/");
     *     System.out.println(resultArray[0] + "," + resultArray[1]); //resultArray[0] => aaaa11111, resultArray[1] => bbbcccc
     */
	public static String[] divide(String target, String cutStr) {
		if (target == null)
			return new String[] { "", "" };
		if (cutStr == null || cutStr.length() == 0)
			return new String[] { target, "" };

		int idx = target.indexOf(cutStr);
		if (idx < 0)
			return new String[] { target, "" };
		else
			return new String[] { target.substring(0, idx),
					target.substring(idx + cutStr.length()) };
	}
	
	
	 /**    
     * 사용예) addTellFormat( "010", "7777", "1234" )<BR>
     * 결   과 ) 02-567-1234<BR><BR>
     *
     * @param oneTellNo 지역번호
     * @param twoTellNo 국번
     * @param thrTellNo 전화번호
     * @return String로 조합된 전화번호 결과
     *    [사용예]
     *    String result = Util.addTellFormat("010", "7754", "2714");
     *    System.out.println(result); //010-7754-2714
     */
	public static String addTellFormat(String oneTellNo, String twoTellNo,
			String thrTellNo) {

		StringBuffer rStr = new StringBuffer();

		try {
			if (oneTellNo == null || oneTellNo.equals(""))
				return "";
			if (twoTellNo == null || twoTellNo.equals(""))
				return "";
			if (thrTellNo == null || thrTellNo.equals(""))
				return "";
			rStr.append(oneTellNo);
			rStr.append("-");
			rStr.append(twoTellNo);
			rStr.append("-");
			rStr.append(thrTellNo);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return rStr.toString().trim();
	}


    /**
     * 사용예) removeTellFormat( "010-7777-1234" )<BR>
     * 결   과 ) String[] removeTell = { "010", "7777", "1234" }<BR><BR>
     *
     * DB에 저장된 전화번호를 "-"를 떼고 String[]으로 
     * 각각 전화번호를 세등분하여 return한다.
     * @param  tellNo
     * @return String[]로 각각 분리된 전화번호 결과
     *    [사용예]
     *    String[] resultArray = Util.removeTellFormat("010-7777-1234");
     *    System.out.println(resultArray[0] + "," + resultArray[1] + "," + resultArray[2]);
     *    //resultArray[0] =>'010',  resultArray[1] => '7777', resultArray[2] => '1234'
     */
	public static String[] removeTellFormat(String tellNo) {
		if (tellNo == null || tellNo.equals("")) {
			return null;
		}
		int first = tellNo.indexOf("-");
		String oneTellNo = tellNo.substring(0, first); // 첫번째 칸 전화번호 ( 02 )

		String twoThrTellNo = tellNo.substring(tellNo.indexOf("-") + 1); // 두번째부터
																			// 뒤까지
																			// 전화번호(567-1234)
		int two = twoThrTellNo.indexOf("-"); //

		String twoTellNo = twoThrTellNo.substring(0, two); // 두번째 칸 전화번호 (567)
		String threeTellNo = twoThrTellNo.substring(two + 1); // 세번째 칸 전화번호
																// (1234)

		String[] removeTell = { oneTellNo, twoTellNo, threeTellNo };

		return removeTell;
	}

	/**
     *  <PRE>
     *  숫자를 천단위로 "," 구분자를 추가하여 변환
     *  ex)"1,234","200,324" 
     *  </PRE>
     *  @param amount
     *  @return 숫자를 천단위로 변환한 결과  
     */
	public static String getAmount(String amount) {

		String convertedAmount = "0";
		if (amount != null && amount.length() != 0) {

			StringBuffer buffer = new StringBuffer();
			for (int i = 0; i < amount.length(); i++) {
				int j = (amount.length() - (i + 1)) % 3;
				if (i != (amount.length() - 1) && j == 0) {
					buffer.append(amount.charAt(i));
					// if(i != 0)
					buffer.append(",");
				} else {
					buffer.append(amount.charAt(i));
				}
			}
			convertedAmount = buffer.toString();
		}
		return convertedAmount;
	}

	
	 /**
	  * @param strTarget
	  * @param loc 삽입할 문자열 위치
	  * @param strInsert 삽입할 문자열
	  * @return 삽입 후 문자열 
	  * 	  [사용예]
	  * 	  rtn = StringUtil.insert("황철연 홍길동", 2, "입니다.");    
	  * 	  System.out.println(rtn); //황철입니다.연 홍길동    
	  */
	public static String insert(String strTarget, int loc, String strInsert) {
		if (strTarget == null) {
			return strInsert;
		}

		if (strInsert == null) {
			return strTarget;
		}

		String result = null;

		try {
			StringBuffer strBuf = new StringBuffer();
			int lengthSize = strTarget.length();
			if (loc >= 0) {
				if (lengthSize < loc) {
					loc = lengthSize;
				}
				strBuf.append(strTarget.substring(0, loc));
				strBuf.append(strInsert);
				strBuf.append(strTarget.substring(loc));
			} else {
				if (lengthSize < Math.abs(loc)) {
					loc = lengthSize * (-1);
				}
				strBuf.append(strTarget.substring(0, (lengthSize - 1) + loc));
				strBuf.append(strInsert);
				strBuf.append(strTarget.substring((lengthSize - 1) + loc
						+ strInsert.length()));
			}
			result = strBuf.toString();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			result = "error";
		}
		return result;
	}

	 /**
     * 문자열을 원하는 길이만큼 지정한 문자로 padding 처리한다.<BR><BR>
     * 사용예) padding("황철연", 15, "0");<BR>
     * 결   과) "황철연000000000000"<BR><BR>
     * @param origin padding 처리할 문자열
     * @param limit padding 처리할 길이
     * @param pad padding 될 문자
     * @return padding 처리된 문자열
     */
	public static String padding(String origin, int limit, String pad) {

		String result;

		String originStr = "";
		if (origin != null) {
			originStr = origin;
		}

		String padStr = "";
		if (pad != null) {
			padStr = pad;
		}

		int size = origin.length();

		if (limit <= size) {
			result = originStr;
		} else {
			StringBuffer sb = new StringBuffer(originStr);

			for (int inx = size; inx < limit; inx++) {
				sb.append(padStr);
			}

			result = sb.toString();
		}
		return result;
	}

	 /**
	    * 문자열을 원하는 길이만큼 지정한 문자로 left padding 처리한다.<BR><BR>
	    * 사용예) leftPadding("황철연", 15, "0");<BR>
	    * 결 과) "00000000황철연입니다."<BR><BR>
	    *
	    * @param origin padding 처리할 문자열
	    * @param limit padding 처리할 길이
	    * @param pad padding 될 문자
	    * @return padding 처리된 문자열
	    */
	public static String leftPadding(String origin, int limit, String pad) {

		String result;
		String temp = pad;

		if (pad == null) {
			temp = "";
		}

		String originStr = "";
		if (origin != null) {
			originStr = origin;
		}

		int size = originStr.length();

		if (limit <= size) {
			result = originStr;
		} else {
			StringBuffer sb = new StringBuffer(temp);
			for (int inx = size + 1; inx < limit; inx++) {
				sb.append(temp);
			}
			result = sb.append(originStr).toString();
		}
		return result;
	}

	 /**
     * 입력된 숫자를 지정된 형태로 출력한다.
     * 숫자가 아닌 값이 들어오면 입력값을 그대로 돌려준다.<BR><BR>
     *
     * 사용예) getFormattedNumber(1, "00000")<BR>
     * 결   과) "00001"<BR><BR>
     *
     * @param pInstr long
     * @return String
     */
   public static String getFormmatedNumber( long num, String format ) {
    
	    String result;
       
		StringBuffer formattedNum = new StringBuffer();
		String strNum = "" + num;

		if (format == null) {
			result = strNum;
		} else {

			try {
				for (int i = 0; i < format.length() - strNum.length(); i++) {
					formattedNum.append(format.charAt(i));
				}
				formattedNum.append(strNum);
			} catch (Exception e) {
				System.out.println(e.getMessage());
				result = "error";
			}
			;
		}

		result = formattedNum.toString();

		return result;
	}
   
   /**
    * 지정된 자릿수만큼 문자열 앞을 메꿔주는 함수.
    * 사용예) blankToString( "522", 5, "#");
    * 결  과) "##522"
    * @param orig 빈칸 채우기 전의 본래 String
    * @param length 문자의 자릿수 int 
    * @param add 빈칸을 채울 문자 String
    * @return 빈칸이 채워진 String
    */
	public static String blankToString(String orig, int length, String add) {

		String buf = "";

		if (orig == null) {
			orig = "";
		}

		int space = length - orig.length();

		int i = 0;
		for (i = 0; i < space; i++)
			buf += add;

		orig = buf + orig;

		return orig;

	}


}
