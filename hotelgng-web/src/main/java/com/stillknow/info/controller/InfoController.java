package com.stillknow.info.controller;


import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stillknow.common.global.utils.DataMap;
import com.stillknow.common.global.utils.DataTableUtil;
import com.stillknow.common.security.domain.CustomUser;
import com.stillknow.info.model.Info;
import com.stillknow.info.service.InfoService;
//import org.springframework.util.StringUtils;

@Controller
@RequestMapping(value="/info/*.do")
public class InfoController<K> {
	
	private static final Logger logger = LoggerFactory.getLogger(InfoController.class);
	
	private ApplicationContext applicationContext;
	
	@Autowired
	private ServletContext servletContext;
	
	@Autowired
	private InfoService infoService;
	
	@RequestMapping
	public @ResponseBody String selectInfoList(DataMap parameters) throws Exception {
		CustomUser user = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println("selectInfoList ::: "+ parameters.toString() );
		System.out.println(parameters.toString());
		Info info = new Gson().fromJson((String) parameters.get("Info"), Info.class);
		System.out.println("selectInfoList ::: info :: "+ info );		
		if(info==null) info=new Info();
		info.setUSER_SEQ(user.getUserSeq());
		info.setUSER_ID(user.getUsername());
		List<Info> list = infoService.selectInfoList(info);
		Map<String,Object> map = new HashMap<>();
		map.put("draw", "1");
		map.put("recordsTotal", list.size());
		map.put("recordsFiltered", list.size());
		map.put("data", list);
		
		String result = new Gson().toJson(map);	
		return result;
	}
	@RequestMapping
	public @ResponseBody String insertInfoList(DataMap parameters) throws Exception {
		CustomUser user = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println("selectInfoList ::: "+ parameters.toString() );
		Type listType = new TypeToken<List<Info>>() {}.getType();
		List<Info> infoList = new Gson().fromJson(DataTableUtil.getParamsAsJson(parameters,new String[]{"CONTENT_SEQ","create"}), listType);
		int i = 0;
		/*
		 	{
			  "fieldErrors": [
			    {
			      "name": "last_name",
			      "status": "This field is required"
			    }
			  ],
			  "data": []
			}
		 */
		List<Info> list = new ArrayList<Info>();
		if(infoList != null && infoList.size() > 0){
			for(Info info : infoList){
				info.setUSER_SEQ(user.getUserSeq());
				info.setUSER_ID(user.getUsername());
				infoService.insertInfo(info);
				list.addAll(infoService.selectInfoList(info));
			}
		}
		Map<String,Object> map = new HashMap<>();
		map.put("draw", "1");
		map.put("recordsTotal", list.size());
		map.put("recordsFiltered", list.size());
		map.put("data", list);
		
		String result = new Gson().toJson(map);
		
		return result;
	}
	@RequestMapping
	public @ResponseBody String updateInfoList(DataMap parameters) throws Exception {
		CustomUser user = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println("selectInfoList ::: "+ parameters.toString() );
		Type listType = new TypeToken<List<Info>>() {}.getType();
		List<Info> infoList = new Gson().fromJson(DataTableUtil.getParamsAsJson(parameters,new String[]{"CONTENT_SEQ","edit"}), listType);
		int i = 0;
		List<Info> list = new ArrayList<Info>();
		if(infoList != null && infoList.size() > 0){
			for(Info info : infoList){
				info.setUSER_SEQ(user.getUserSeq());
				info.setUSER_ID(user.getUsername());
				i = infoService.updateInfo(info);
				list.addAll(infoService.selectInfoList(info));
			}
		}
		Map<String,Object> map = new HashMap<>();
		map.put("draw", "1");
		map.put("recordsTotal", list.size());
		map.put("recordsFiltered", list.size());
		map.put("data", list);
		
		String result = new Gson().toJson(map);
		
		return result;
	}
	@RequestMapping
	public @ResponseBody String deleteInfoList(DataMap parameters) throws Exception {
		CustomUser user = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println("selectInfoList ::: "+ parameters.toString() );
		Type listType = new TypeToken<List<Info>>() {}.getType();
		List<Info> infoList = new Gson().fromJson(DataTableUtil.getParamsAsJson(parameters,new String[]{"CONTENT_SEQ","remove"}), listType);
		int i = 0;
		List<Info> list = new ArrayList<Info>();
		if(infoList != null && infoList.size() > 0){
			for(Info info : infoList){
				info.setUSER_SEQ(user.getUserSeq());
				info.setUSER_ID(user.getUsername());
				i = infoService.deleteInfo(info);
			}
		}
		Map<String,Object> map = new HashMap<>();
		String result = new Gson().toJson(map);
		
		return result;
	}
	
}
