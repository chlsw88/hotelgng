package com.stillknow.info.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stillknow.info.model.Info;
import com.stillknow.info.persistence.InfoMapper;

@Service("InfoService")
public class InfoService{
		
	@Autowired
	private InfoMapper infoMapper;

	public List<Info> selectInfoList(Info info){
		return infoMapper.selectInfoList(info);
	}
	public void insertInfo(Info info){
		infoMapper.insertInfo(info);
	}
	public int updateInfo(Info info){
		return infoMapper.updateInfo(info);
	}
	public int deleteInfo(Info info){
		return infoMapper.deleteInfo(info);
	}
}
