package com.stillknow.info.persistence;

import java.util.List;

import com.stillknow.info.model.Info;

public interface InfoMapper {
		
	public List<Info> selectInfoList(Info info);
	public int insertInfo(Info info);
	public int updateInfo(Info info);
	public int deleteInfo(Info info);
}
