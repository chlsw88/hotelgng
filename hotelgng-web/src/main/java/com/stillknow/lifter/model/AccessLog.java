package com.stillknow.lifter.model;

import java.io.Serializable;

public class AccessLog implements Serializable {
	private static final long serialVersionUID = 4452766197421076759L;
	
//    ,PHONE_CONTENT_SEQ	int(10)
//    ,ACCESS_TYPE		int(2)			/* 1: 초기진입, 2: ROUTE종료, 3: 기타 */
//    ,PAGE_NUM			varchar(2)		/* 0: 페이지 분실, 기타: 접속 했던 페이지 */
//    ,ELAPSED_MS			int(6)		/* 총 한 루트 걸린 시간 : 밀리 초 단위 */
//    ,DELAY_MS			int(5)		/* 컨텐츠 체류시간 : 밀리 초 단위 */
//	,IPV4				varchar(2)
//	,REG_ID				varchar(30)
//	,REG_DT				DATETIME		default NOW()
	private String username;
	private String password;


	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "User [id=" + username + ", password=" + password + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AccessLog other = (AccessLog) obj;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

}
