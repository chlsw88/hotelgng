package com.stillknow.lifter.model;

import java.io.Serializable;

/**
 * @author	: Administrator
 * @date	: 2016. 5. 22. 오후 11:59:22
 * @project : lifter
 * @package : com.stillknow.lifter.model
 * @file	: Content.java
 */
public class Statistic extends Lifter implements Serializable {
	private static final long serialVersionUID = 4452766197421076759L;
	
	/*
		CONTENT_PAGE_SEQ
		CONTENT_SEQ
		USER_SEQ
		PAGE_NUM
		PAGE_RANK
		PHONE_SEQ
		REG_DT
	 */
	private String CONTENT_TITLE;
	private String REG_DT;
	private String HOURS;
	private String PAGE_NUM;
	private String PAGE_RANK;
	private String CNT;
	public String getCONTENT_TITLE() {
		return CONTENT_TITLE;
	}
	public void setCONTENT_TITLE(String cONTENT_TITLE) {
		CONTENT_TITLE = cONTENT_TITLE;
	}
	public String getREG_DT() {
		return REG_DT;
	}
	public void setREG_DT(String rEG_DT) {
		REG_DT = rEG_DT;
	}
	public String getHOURS() {
		return HOURS;
	}
	public void setHOURS(String hOURS) {
		HOURS = hOURS;
	}
	public String getPAGE_NUM() {
		return PAGE_NUM;
	}
	public void setPAGE_NUM(String pAGE_NUM) {
		PAGE_NUM = pAGE_NUM;
	}
	public String getPAGE_RANK() {
		return PAGE_RANK;
	}
	public void setPAGE_RANK(String pAGE_RANK) {
		PAGE_RANK = pAGE_RANK;
	}
	public String getCNT() {
		return CNT;
	}
	public void setCNT(String cNT) {
		CNT = cNT;
	}
}
