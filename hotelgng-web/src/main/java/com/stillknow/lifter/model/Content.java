package com.stillknow.lifter.model;

import java.io.Serializable;

/**
 * @author	: Administrator
 * @date	: 2016. 5. 22. 오후 11:59:22
 * @project : lifter
 * @package : com.stillknow.lifter.model
 * @file	: Content.java
 */
public class Content extends Lifter implements Serializable {
	private static final long serialVersionUID = 4452766197421076759L;
	
	/*
		CONTENT_SEQ
		,USER_SEQ
		,CONTENT_KEYWORD
		,CONTENT_TITLE
		,CONTENT_URL
		,DELAY_TM
		,DELAY_RANGE
		,PAGE_NUM
		,PAGE_RANK
		,PRIOR
		,USE_YN
		,DEL_YN
		,REG_ID
		,REG_DT
		,MOD_ID
		,MOD_DT
	 */
	private String CONTENT_SEQ;
	private String CONTENT_KEYWORD;
	private String CONTENT_TITLE;
	private String CONTENT_URL;
	private String DELAY_TM;
	private String DELAY_RANGE;
	private String PAGE_NUM;
	private String PAGE_RANK;
	private String PRIOR;
	private String USE_YN;
	private String DEL_YN;
	
	@Override
	public String toString() {
		return "";//"User [id=" + username + ", password=" + password + "]";
	}

	public String getCONTENT_SEQ() {
		return CONTENT_SEQ;
	}

	public void setCONTENT_SEQ(String cONTENT_SEQ) {
		CONTENT_SEQ = cONTENT_SEQ;
	}

	public String getCONTENT_KEYWORD() {
		return CONTENT_KEYWORD;
	}

	public void setCONTENT_KEYWORD(String cONTENT_KEYWORD) {
		CONTENT_KEYWORD = cONTENT_KEYWORD;
	}

	public String getCONTENT_TITLE() {
		return CONTENT_TITLE;
	}

	public void setCONTENT_TITLE(String cONTENT_TITLE) {
		CONTENT_TITLE = cONTENT_TITLE;
	}

	public String getCONTENT_URL() {
		return CONTENT_URL;
	}

	public void setCONTENT_URL(String cONTENT_URL) {
		CONTENT_URL = cONTENT_URL;
	}

	public String getDELAY_TM() {
		return DELAY_TM;
	}

	public void setDELAY_TM(String dELAY_TM) {
		DELAY_TM = dELAY_TM;
	}

	public String getDELAY_RANGE() {
		return DELAY_RANGE;
	}

	public void setDELAY_RANGE(String dELAY_RANGE) {
		DELAY_RANGE = dELAY_RANGE;
	}

	public String getPAGE_NUM() {
		return PAGE_NUM;
	}

	public void setPAGE_NUM(String pAGE_NUM) {
		PAGE_NUM = pAGE_NUM;
	}

	public String getPAGE_RANK() {
		return PAGE_RANK;
	}

	public void setPAGE_RANK(String pAGE_RANK) {
		PAGE_RANK = pAGE_RANK;
	}

	public String getPRIOR() {
		return PRIOR;
	}

	public void setPRIOR(String pRIOR) {
		PRIOR = pRIOR;
	}

	public String getUSE_YN() {
		return USE_YN;
	}

	public void setUSE_YN(String uSE_YN) {
		USE_YN = uSE_YN;
	}

	public String getDEL_YN() {
		return DEL_YN;
	}

	public void setDEL_YN(String dEL_YN) {
		DEL_YN = dEL_YN;
	}

}
