package com.stillknow.lifter.model;

import java.io.Serializable;

public class ContentPage implements Serializable {
	private static final long serialVersionUID = 4452766197421076759L;

	/*
	 	CONTENT_SEQ
		USER_SEQ  
		PAGE_NUM  
		PAGE_RANK 
		PHONE_SEQ 
	 */
	private String CONTENT_SEQ;
	private String CONTENT_KEYWORD;
	private String CONTENT_URL;
	private String USER_SEQ;
	private String PAGE_NUM;
	private String PAGE_RANK;
	private String PHONE_SEQ;
	private String PHONE_NUMBER;
	private String USIM;

	@Override
	public String toString() {
		return "ContentPage [CONTENT_SEQ=" + CONTENT_SEQ + ", USER_SEQ=" + USER_SEQ + "]";
	}

	public String getCONTENT_SEQ() {
		return CONTENT_SEQ;
	}
	
	public String getCONTENT_KEYWORD() {
		return CONTENT_KEYWORD;
	}

	public void setCONTENT_KEYWORD(String cONTENT_KEYWORD) {
		CONTENT_KEYWORD = cONTENT_KEYWORD;
	}

	public String getCONTENT_URL() {
		return CONTENT_URL;
	}

	public void setCONTENT_URL(String cONTENT_URL) {
		CONTENT_URL = cONTENT_URL;
	}

	public void setCONTENT_SEQ(String CONTENT_SEQ) {
		this.CONTENT_SEQ = CONTENT_SEQ;
	}

	public String getUSER_SEQ() {
		return USER_SEQ;
	}

	public void setUSER_SEQ(String USER_SEQ) {
		this.USER_SEQ = USER_SEQ;
	}

	public String getPAGE_NUM() {
		return PAGE_NUM;
	}

	public void setPAGE_NUM(String PAGE_NUM) {
		this.PAGE_NUM = PAGE_NUM;
	}

	public String getPAGE_RANK() {
		return PAGE_RANK;
	}

	public void setPAGE_RANK(String PAGE_RANK) {
		this.PAGE_RANK = PAGE_RANK;
	}

	public String getPHONE_SEQ() {
		return PHONE_SEQ;
	}

	public void setPHONE_SEQ(String PHONE_SEQ) {
		this.PHONE_SEQ = PHONE_SEQ;
	}

	public String getPHONE_NUMBER() {
		return PHONE_NUMBER;
	}

	public void setPHONE_NUMBER(String pHONE_NUMBER) {
		PHONE_NUMBER = pHONE_NUMBER;
	}

	public String getUSIM() {
		return USIM;
	}

	public void setUSIM(String uSIM) {
		USIM = uSIM;
	}
}
