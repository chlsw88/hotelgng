package com.stillknow.lifter.model;

import java.io.Serializable;

public class Phone extends Lifter implements Serializable {
	private static final long serialVersionUID = 4452766197421076759L;
	/*
	 PHONE_SEQ
	,USER_SEQ
	,MAKER
	,USIM
	,PHONE_CD
	,PHONE_NUMBER
	,USING_YN
	,INACTIVE_OK_MIN
	,REGULAR_BOOT_MIN
	 */
	private String PHONE_SEQ; 
	private String PHONE_NAME;
	private String MAKER; 
	private String USIM; 
	private String PHONE_CD; 
	private String PHONE_NUMBER; 
	private String USING_YN; 
	private String INACTIVE_OK_MIN; 
	private String REGULAR_BOOT_MIN;
	private String DEL_YN;
	private String ALLOWED_YN;
	private BootHist bootHist;

	@Override
	public String toString() {
		return "Phone [PHONE_SEQ=" + PHONE_SEQ 
				+ ", USER_SEQ=" + USER_SEQ 
				+ ", USER_ID=" + USER_ID 
				+ ", PHONE_NAME=" + PHONE_NAME 
				+ ", MAKER=" + MAKER 
				+ ", USIM=" + USIM 
				+ ", PHONE_CD=" + PHONE_CD 
				+ ", PHONE_NUMBER=" + PHONE_NUMBER 
				+ ", USING_YN=" + USING_YN 
				+ ", INACTIVE_OK_MIN=" + INACTIVE_OK_MIN 
				+ ", REGULAR_BOOT_MIN=" + REGULAR_BOOT_MIN 
				+ ", DEL_YN=" + DEL_YN 
				+ ", ALLOWED_YN=" + ALLOWED_YN 
				+ "]"
				;
	}

	public String getPHONE_SEQ() {
		return PHONE_SEQ;
	}

	public void setPHONE_SEQ(String pHONE_SEQ) {
		PHONE_SEQ = pHONE_SEQ;
	}

	public String getUSER_SEQ() {
		return USER_SEQ;
	}

	public void setUSER_SEQ(String uSER_SEQ) {
		USER_SEQ = uSER_SEQ;
	}
	
	public String getPHONE_NAME() {
		return PHONE_NAME;
	}

	public void setPHONE_NAME(String pHONE_NAME) {
		PHONE_NAME = pHONE_NAME;
	}

	public String getUSER_ID() {
		return USER_ID;
	}

	public void setUSER_ID(String uSER_ID) {
		USER_ID = uSER_ID;
	}

	public String getMAKER() {
		return MAKER;
	}

	public void setMAKER(String mAKER) {
		MAKER = mAKER;
	}

	public String getUSIM() {
		return USIM;
	}

	public void setUSIM(String uSIM) {
		USIM = uSIM;
	}

	public String getPHONE_CD() {
		return PHONE_CD;
	}

	public void setPHONE_CD(String pHONE_CD) {
		PHONE_CD = pHONE_CD;
	}

	public String getPHONE_NUMBER() {
		return PHONE_NUMBER;
	}

	public void setPHONE_NUMBER(String pHONE_NUMBER) {
		PHONE_NUMBER = pHONE_NUMBER;
	}

	public String getUSING_YN() {
		return USING_YN;
	}

	public void setUSING_YN(String uSING_YN) {
		USING_YN = uSING_YN;
	}

	public String getINACTIVE_OK_MIN() {
		return INACTIVE_OK_MIN;
	}

	public void setINACTIVE_OK_MIN(String iNACTIVE_OK_MIN) {
		INACTIVE_OK_MIN = iNACTIVE_OK_MIN;
	}

	public String getREGULAR_BOOT_MIN() {
		return REGULAR_BOOT_MIN;
	}

	public void setREGULAR_BOOT_MIN(String rEGULAR_BOOT_MIN) {
		REGULAR_BOOT_MIN = rEGULAR_BOOT_MIN;
	}

	public String getDEL_YN() {
		return DEL_YN;
	}

	public void setDEL_YN(String dEL_YN) {
		DEL_YN = dEL_YN;
	}

	public String getALLOWED_YN() {
		return ALLOWED_YN;
	}

	public void setALLOWED_YN(String aLLOWED_YN) {
		ALLOWED_YN = aLLOWED_YN;
	}
	
}
