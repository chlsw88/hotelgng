package com.stillknow.lifter.model;

import java.io.Serializable;

/**
 * @author	: Administrator
 * @date	: 2016. 5. 27. 오후 2:08:43
 * @project : lifter
 * @package : com.stillknow.lifter.model
 * @file	: PhoneContent.java
 */
public class PhoneContent extends Lifter implements Serializable {
	private static final long serialVersionUID = 4452766197421076759L;
	private String PHONE_CONTENT_SEQ; 
	private String PHONE_SEQ; 
	private String CONTENT_SEQ; 
	private String USER_SEQ;
	private String PHONE_NAME;
	private String CONTENT_TITLE;
	public String getPHONE_CONTENT_SEQ() {
		return PHONE_CONTENT_SEQ;
	}
	public void setPHONE_CONTENT_SEQ(String pHONE_CONTENT_SEQ) {
		PHONE_CONTENT_SEQ = pHONE_CONTENT_SEQ;
	}
	public String getPHONE_SEQ() {
		return PHONE_SEQ;
	}
	public void setPHONE_SEQ(String pHONE_SEQ) {
		PHONE_SEQ = pHONE_SEQ;
	}
	public String getCONTENT_SEQ() {
		return CONTENT_SEQ;
	}
	public void setCONTENT_SEQ(String cONTENT_SEQ) {
		CONTENT_SEQ = cONTENT_SEQ;
	}
	public String getUSER_SEQ() {
		return USER_SEQ;
	}
	public void setUSER_SEQ(String uSER_SEQ) {
		USER_SEQ = uSER_SEQ;
	}
	public String getPHONE_NAME() {
		return PHONE_NAME;
	}
	public void setPHONE_NAME(String pHONE_NAME) {
		PHONE_NAME = pHONE_NAME;
	}
	public String getCONTENT_TITLE() {
		return CONTENT_TITLE;
	}
	public void setCONTENT_TITLE(String cONTENT_TITLE) {
		CONTENT_TITLE = cONTENT_TITLE;
	}
}
