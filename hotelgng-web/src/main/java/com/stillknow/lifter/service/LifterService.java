package com.stillknow.lifter.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stillknow.lifter.model.Phone;
import com.stillknow.lifter.persistence.LifterMapper;

@Service("LifterService")
public class LifterService{
		
	@Autowired
	private LifterMapper lifterMapper;
	/*
	public void createLoginHist(LoginHistory loginHistory) {
		loginHistoryMapper.createLoginHist(loginHistory);
	}
	*/
	public String test(String str){
		return lifterMapper.test(str);
	}
	public List<Phone> selectUserPhoneList(Phone phone){
		return lifterMapper.selectUserPhoneList(phone);
	}
	public void insertUserPhone(Phone phone){
		 lifterMapper.insertUserPhone(phone);
	}
	public int updateUserPhone(Phone phone){
		return lifterMapper.updateUserPhone(phone);
	}
	public int deleteUserPhone(Phone phone){
		return lifterMapper.deleteUserPhone(phone);
	}
}
