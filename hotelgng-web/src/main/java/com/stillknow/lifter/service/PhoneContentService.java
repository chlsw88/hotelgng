package com.stillknow.lifter.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stillknow.lifter.model.Content;
import com.stillknow.lifter.model.Phone;
import com.stillknow.lifter.model.PhoneContent;
import com.stillknow.lifter.persistence.PhoneContentMapper;

/**
 * @author	: Administrator
 * @date	: 2016. 5. 27. 오후 1:58:52
 * @project : lifter
 * @package : com.stillknow.lifter.service
 * @file	: PhoneContentService.java
 */
@Service("PhoneContentService")
public class PhoneContentService{
		
	@Autowired
	private PhoneContentMapper phoneContentMapper;
	public List<PhoneContent> selectPhoneContentList(PhoneContent phoneContent){
		return phoneContentMapper.selectPhoneContentList(phoneContent);
	}
	public List<Phone> selectPhoneList(Phone phone){
		return phoneContentMapper.selectPhoneList(phone);
	}
	public List<Content> selectContentList(Content content){
		return phoneContentMapper.selectContentList(content);
	}
	public void insertPhoneContent(PhoneContent phoneContent){
		 phoneContentMapper.insertPhoneContent(phoneContent);
	}
	public int updatePhoneContent(PhoneContent phoneContent){
		return phoneContentMapper.updatePhoneContent(phoneContent);
	}
	public int deletePhoneContent(PhoneContent phoneContent){
		return phoneContentMapper.deletePhoneContent(phoneContent);
	}
}
