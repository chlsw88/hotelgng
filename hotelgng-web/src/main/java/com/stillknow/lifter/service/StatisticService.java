package com.stillknow.lifter.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stillknow.lifter.model.Statistic;
import com.stillknow.lifter.persistence.StatisticMapper;

@Service("StatisticService")
public class StatisticService{
		
	@Autowired
	private StatisticMapper statisticMapper;

	public List<Statistic> selectTempStatisticList(Statistic statistic){
		return statisticMapper.selectTempStatisticList(statistic);
	}
}
