package com.stillknow.lifter.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stillknow.lifter.model.Content;
import com.stillknow.lifter.persistence.ContentMapper;

@Service("ContentService")
public class ContentService{
		
	@Autowired
	private ContentMapper contentMapper;

	public List<Content> selectContentList(Content content){
		return contentMapper.selectContentList(content);
	}
	public void insertContent(Content content){
		 contentMapper.insertContent(content);
	}
	public int updateContent(Content content){
		return contentMapper.updateContent(content);
	}
	public int deleteContent(Content content){
		return contentMapper.deleteContent(content);
	}
}
