package com.stillknow.lifter.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stillknow.common.global.utils.DataMap;
import com.stillknow.lifter.model.Content;
import com.stillknow.lifter.model.ContentPage;
import com.stillknow.lifter.persistence.DeviceMapper;

@Service("DeviceService")
public class DeviceService{
		
	@Autowired
	private DeviceMapper deviceMapper;
	public List<DataMap> getContentInfo(DataMap dataMap){
		return deviceMapper.getContentInfo(dataMap);
	}
	public List<DataMap> getContentPageList(DataMap dataMap){
		return deviceMapper.getContentPageList(dataMap);
	}
	public int insertAccessLog(DataMap dataMap){
		return deviceMapper.insertAccessLog(dataMap);
	}
	public int insertContentPage(ContentPage contentPage){
		return deviceMapper.insertContentPage(contentPage);
	}
	public int updateContent(ContentPage contentPage){
		return deviceMapper.updateContent(contentPage);
	}
}
