package com.stillknow.lifter.persistence;

import java.util.List;

import com.stillknow.lifter.model.Phone;

public interface LifterMapper {
		
	/*
	public List<LoginHistory> selectLoginHistory();
	public List<LoginHistory> selectLoginHistory(DataMap parameters);
	public void createLoginHist(LoginHistory loginHistory);
	public void createLogoutHist(LoginHistory loginHistory);
	*/
	public String test(String str);
	public List<Phone> selectUserPhoneList(Phone phone);
	public int insertUserPhone(Phone phone);
	public int updateUserPhone(Phone phone);
	public int deleteUserPhone(Phone phone);
}
