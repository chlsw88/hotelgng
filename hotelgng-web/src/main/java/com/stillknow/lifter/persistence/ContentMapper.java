package com.stillknow.lifter.persistence;

import java.util.List;

import com.stillknow.lifter.model.Content;

public interface ContentMapper {
		
	public List<Content> selectContentList(Content content);
	public int insertContent(Content content);
	public int updateContent(Content content);
	public int deleteContent(Content content);
}
