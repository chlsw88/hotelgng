package com.stillknow.lifter.persistence;

import java.util.List;

import com.stillknow.common.global.utils.DataMap;
import com.stillknow.lifter.model.Content;
import com.stillknow.lifter.model.ContentPage;

public interface DeviceMapper {
		
	public List<DataMap> getContentInfo(DataMap dataMap);
	public List<DataMap> getContentPageList(DataMap dataMap);
	public int insertAccessLog(DataMap dataMap);
	public int insertContentPage(ContentPage ContentPage);
	public int updateContent(ContentPage contentPage);
}
