package com.stillknow.lifter.persistence;

import java.util.List;

import com.stillknow.lifter.model.Statistic;

public interface StatisticMapper {
		
	public List<Statistic> selectTempStatisticList(Statistic statistic);
}
