package com.stillknow.lifter.persistence;

import java.util.List;

import com.stillknow.lifter.model.Content;
import com.stillknow.lifter.model.Phone;
import com.stillknow.lifter.model.PhoneContent;

/**
 * @author	: Administrator
 * @date	: 2016. 5. 27. 오후 1:55:25
 * @project : lifter
 * @package : com.stillknow.lifter.persistence
 * @file	: PhoneContentMapper.java
 */
public interface PhoneContentMapper {
	
	public List<PhoneContent> selectPhoneContentList(PhoneContent phoneContent);
	public List<Phone> selectPhoneList(Phone phone);
	public List<Content> selectContentList(Content content);
	public int insertPhoneContent(PhoneContent phoneContent);
	public int updatePhoneContent(PhoneContent phoneContent);
	public int deletePhoneContent(PhoneContent phoneContent);
	
}
