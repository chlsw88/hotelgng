package com.stillknow.lifter.controller;


import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.view.RedirectView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stillknow.common.global.utils.DataMap;
import com.stillknow.common.global.utils.DataTableUtil;
import com.stillknow.common.security.domain.CustomUser;
import com.stillknow.lifter.model.ContentPage;
import com.stillknow.lifter.model.Phone;
import com.stillknow.lifter.service.LifterService;
//import org.springframework.util.StringUtils;

@Controller
@RequestMapping(value="/pn/*.do")
public class LifterController {
	
	private static final Logger logger = LoggerFactory.getLogger(LifterController.class);
	
	private ApplicationContext applicationContext;
	
	@Autowired
	private ServletContext servletContext;
	
	@Autowired
	private LifterService lifterService;
	
	/*
	@RequestMapping
	public ModelAndView 
	
	*/

	@RequestMapping
	public @ResponseBody String test(DataMap parameters) throws Exception {
		
		RedirectView rdrv = null;
		if( rdrv == null ){
			rdrv = new RedirectView("/html/common/login/login_failure.html");
		}
		String result = lifterService.test("test");
		System.out.println("result");
		DataMap<String,Object> dataMap = new DataMap();
		dataMap.put("result", result);
		rdrv.setAttributesMap(dataMap);
		return result;
	}
	@RequestMapping
	public @ResponseBody String selectUserPhoneList(DataMap parameters) throws Exception {
		CustomUser user = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println("selectUserPhoneList ::: "+ parameters.toString() );
		Phone phone = new Gson().fromJson((String) parameters.get("PHONE"), Phone.class);
		if(phone==null) phone=new Phone();
		phone.setUSER_SEQ(user.getUserSeq());
		phone.setUSER_ID(user.getUsername());
		List<Phone> list = lifterService.selectUserPhoneList(phone);
		Map<String,Object> map = new HashMap<>();
		map.put("draw", "1");
		map.put("recordsTotal", list.size());
		map.put("recordsFiltered", list.size());
		map.put("data", DataTableUtil.putListWithAuth(list, user));
		
		String result = new Gson().toJson(map);		
		return result;
	}
	@RequestMapping
	public @ResponseBody String insertUserPhoneList(DataMap parameters) throws Exception {
		CustomUser user = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println("selectUserPhoneList ::: "+ parameters.toString() );
		Type listType = new TypeToken<List<Phone>>() {}.getType();
		List<Phone> phoneList = new Gson().fromJson((String) parameters.get("PHONE"), listType);
		int i = 0;
		if(phoneList != null && phoneList.size() > 0){
			for(Phone phone : phoneList){
				phone.setUSER_SEQ(user.getUserSeq());
				phone.setUSER_ID(user.getUsername());
				lifterService.insertUserPhone(phone);
				i=1;
			}
		}
		return i+"";
	}
	@RequestMapping
	public @ResponseBody String updateUserPhoneList(DataMap parameters) throws Exception {
		CustomUser user = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println("selectUserPhoneList ::: "+ parameters.toString() );
		Type listType = new TypeToken<List<Phone>>() {}.getType();
		List<Phone> phoneList = new Gson().fromJson((String) parameters.get("PHONE"), listType);
		int i = 0;
		if(phoneList != null && phoneList.size() > 0){
			for(Phone phone : phoneList){
				phone.setUSER_SEQ(user.getUserSeq());
				phone.setUSER_ID(user.getUsername());
				i = lifterService.updateUserPhone(phone);
			}
		}
				
		return i+"";
	}
	@RequestMapping
	public @ResponseBody String deleteUserPhoneList(DataMap parameters) throws Exception {
		CustomUser user = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println("selectUserPhoneList ::: "+ parameters.toString() );
		Type listType = new TypeToken<List<Phone>>() {}.getType();
		List<Phone> phoneList = new Gson().fromJson((String) parameters.get("PHONE"), listType);
		int i = 0;
		if(phoneList != null && phoneList.size() > 0){
			for(Phone phone : phoneList){
				phone.setUSER_SEQ(user.getUserSeq());
				phone.setUSER_ID(user.getUsername());
				i = lifterService.deleteUserPhone(phone);
			}
		}
		return i+"";
	}
	
}
