package com.stillknow.lifter.controller;


import java.lang.reflect.Type;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stillknow.common.global.utils.DataMap;
import com.stillknow.lifter.model.ContentPage;
import com.stillknow.lifter.model.Phone;
import com.stillknow.lifter.service.DeviceService;
//import org.springframework.util.StringUtils;

@Controller
@RequestMapping(value="/device/*.do")
public class DeviceController {
	
	private static final Logger logger = LoggerFactory.getLogger(DeviceController.class);
	
	private ApplicationContext applicationContext;
	
	@Autowired
	private ServletContext servletContext;
	
	@Autowired
	private DeviceService deviceService;
	
	@RequestMapping
	public @ResponseBody String getContentInfo(DataMap parameters) throws Exception {
		/* TODO 유저 및 휴대폰 유효 확인 */	
		//deviceService.isValidAccess(parameters);
		
		List<DataMap> list = null;
		System.out.println("parameters.getString('PHONE_NUMBER') ::: "+ parameters.getString("PHONE_NUMBER"));
		if( parameters.getString("PHONE_NUMBER")!=null && !"".equals(parameters.getString("PHONE_NUMBER"))
			&& parameters.getString("USIM")!=null && !"".equals(parameters.getString("USIM")) ){
			// TODO 지우기 임시용
			list = deviceService.getContentInfo(parameters);
		}
		
		
		// TODO 전에 접속한거랑 ip 비교 같으면 비행기 모드 작업
		
		System.out.println("parameters ::: "+ parameters);
		int random =0;
		String result = "";
		DataMap map = new DataMap();
		if(list != null && list.size() > 0){
			random = new Random().nextInt(list.size() > 1?2:1)+1;
			for(DataMap tempMap : list){
				if((random+".0").equals(tempMap.getString("RN")) || (random+"").equals(tempMap.getString("RN")) ){
					map.putAll(tempMap);				
					map.put("resultCd", "00");
					map.put("resultMsg", "성공적으로 조회 되었습니다.");
					break;
				}
			}
			//조회 성공 접근 저장
		} else {
			map.put("resultCd", "01");
			map.put("resultMsg", "등록된 키워드가 없습니다.");
		}
		
		/* TODO 요청 접근 로그 남기기 */	
		parameters.put("ACCESS_TYPE", "1");		// 1: 작업조회, 2:작업종료, 3:페이지검사 할 것 조회, 4:페이지갱신
		parameters.put("PHONE_CONTENT_SEQ", map.get("PHONE_CONTENT_SEQ")); 
		parameters.put("PAGE_NUM",map.get("PAGE_NUM")); 
		deviceService.insertAccessLog(parameters);
		
		result = new Gson().toJson(map);
		return result;
	}
	
	@RequestMapping
	public @ResponseBody String insertAccessLog(DataMap parameters) throws Exception {
		/* TODO 유저 및 휴대폰 유효 확인 */	
		//deviceService.isValidAccess(parameters);
		
		List<DataMap> list = null;
		int cnt = 0;
		DataMap map = new DataMap();
		System.out.println("parameters.getString('PHONE_CONTENT_SEQ') ::: "+ parameters.getString("PHONE_CONTENT_SEQ"));
		if( parameters.getString("PHONE_CONTENT_SEQ")!=null && !"".equals(parameters.getString("PHONE_CONTENT_SEQ")) 
				&& parameters.getString("PHONE_NUMBER")!=null && !"".equals(parameters.getString("PHONE_NUMBER"))
				&& parameters.getString("USIM")!=null && !"".equals(parameters.getString("USIM")) ){
			cnt = deviceService.insertAccessLog(parameters);
		} else {
			
		}
		//PHONE_CONTENT_SEQ,PHONE_NUMBER,USIM,ACCESS_TYPE,ELAPSED_MS,DELAY_MS,PAGE_NUM,IPV4,ACCESS_TYPE
		
		// TODO 부팅 해야 되는지, 비행기 모드 해야되는지 등등 
		System.out.println("parameters ::: "+ parameters);
		int random =0;
		String result = "";

		if( cnt > 0){
			//random = new Random().nextInt(list.size() > 1?2:1);
			//map = list.get(random);
			map.put("resultCd", "00");
			map.put("resultMsg", "성공적으로 조회 되었습니다.");
			//조회 성공 접근 저장
		} else {
			map = new DataMap();
			map.put("resultCd", "01");
			map.put("resultMsg", "등록된 키워드가 없습니다.");
		}
		
		/* TODO 요청 접근 로그 남기기 */
		
		result = new Gson().toJson(map);
		return result;
	}
	@RequestMapping
	public @ResponseBody String getContentPageList(DataMap parameters) throws Exception {
		List<DataMap> list = null;
		System.out.println("parameters.getString('PHONE_NUMBER') ::: "+ parameters.getString("PHONE_NUMBER"));
		if( parameters.getString("PHONE_NUMBER")!=null && !"".equals(parameters.getString("PHONE_NUMBER"))
			&& parameters.getString("USIM")!=null && !"".equals(parameters.getString("USIM")) ){
			// TODO 지우기 임시용
			list = deviceService.getContentPageList(parameters);
		}
		
		
		// TODO 전에 접속한거랑 ip 비교 같으면 비행기 모드 작업
		
		System.out.println("parameters ::: "+ parameters);
		int random =0;
		String result = "";
		DataMap map;
		if(list != null && list.size() > 0){
			random = new Random().nextInt(list.size() > 1?2:1);
			map = list.get(random);
			map.put("resultCd", "00");
			map.put("resultMsg", "성공적으로 조회 되었습니다.");
			//조회 성공 접근 저장
		} else {
			map = new DataMap();
			map.put("resultCd", "01");
			map.put("resultMsg", "등록된 키워드가 없습니다.");
		}
		
		/* TODO 요청 접근 로그 남기기 */	
		parameters.put("ACCESS_TYPE", "3");		// 1: 작업조회, 2:작업종료, 3:페이지검사 할 것 조회, 4:페이지갱신
		parameters.put("PHONE_CONTENT_SEQ", 0); //Content 순위 체크하는 개수
		parameters.put("PAGE_NUM",list != null && list.size() > 0 ? list.size() : 0); 
		deviceService.insertAccessLog(parameters);
		
		result = new Gson().toJson(list);
		
		return result;
	}
	@RequestMapping
	public @ResponseBody String insertContentPageList(DataMap parameters) throws Exception {
		Type listType = new TypeToken<List<ContentPage>>() {}.getType();
		List<ContentPage> contentRankList = new Gson().fromJson((String) parameters.get("CONTENT_PAGE_LIST"), listType);
		if(contentRankList != null && contentRankList.size() > 0){
			for(ContentPage contentPage : contentRankList){
				deviceService.insertContentPage(contentPage);
				deviceService.updateContent(contentPage);
			}
		}
		return null;
	}
}
