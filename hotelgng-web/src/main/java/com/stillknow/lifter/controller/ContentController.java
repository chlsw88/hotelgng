package com.stillknow.lifter.controller;


import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.stillknow.common.global.utils.DataMap;
import com.stillknow.common.global.utils.DataTableUtil;
import com.stillknow.common.security.domain.CustomUser;
import com.stillknow.lifter.model.Content;
import com.stillknow.lifter.service.ContentService;
//import org.springframework.util.StringUtils;

@Controller
@RequestMapping(value="/pn/content/*.do")
public class ContentController<K> {
	
	private static final Logger logger = LoggerFactory.getLogger(ContentController.class);
	
	private ApplicationContext applicationContext;
	
	@Autowired
	private ServletContext servletContext;
	
	@Autowired
	private ContentService contentService;
	
	@RequestMapping
	public @ResponseBody String selectContentList(DataMap parameters) throws Exception {
		CustomUser user = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println("selectContentList ::: "+ parameters.toString() );
		System.out.println(parameters.toString());
		Content content = new Gson().fromJson((String) parameters.get("Content"), Content.class);
		System.out.println("selectContentList ::: content :: "+ content );		
		if(content==null) content=new Content();
		content.setUSER_SEQ(user.getUserSeq());
		content.setUSER_ID(user.getUsername());
		List<Content> list = contentService.selectContentList(content);
		Map<String,Object> map = new HashMap<>();
		map.put("draw", "1");
		map.put("recordsTotal", list.size());
		map.put("recordsFiltered", list.size());
		map.put("data", list);
		
		String result = new Gson().toJson(map);	
		return result;
	}
	@RequestMapping
	public @ResponseBody String insertContentList(DataMap parameters) throws Exception {
		CustomUser user = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println("selectContentList ::: "+ parameters.toString() );
		Type listType = new TypeToken<List<Content>>() {}.getType();
		List<Content> contentList = new Gson().fromJson(DataTableUtil.getParamsAsJson(parameters,new String[]{"CONTENT_SEQ","create"}), listType);
		int i = 0;
		/*
		 	{
			  "fieldErrors": [
			    {
			      "name": "last_name",
			      "status": "This field is required"
			    }
			  ],
			  "data": []
			}
		 */
		List<Content> list = new ArrayList<Content>();
		if(contentList != null && contentList.size() > 0){
			for(Content content : contentList){
				content.setUSER_SEQ(user.getUserSeq());
				content.setUSER_ID(user.getUsername());
				contentService.insertContent(content);
				list.addAll(contentService.selectContentList(content));
			}
		}
		Map<String,Object> map = new HashMap<>();
		map.put("draw", "1");
		map.put("recordsTotal", list.size());
		map.put("recordsFiltered", list.size());
		map.put("data", list);
		
		String result = new Gson().toJson(map);
		
		return result;
	}
	@RequestMapping
	public @ResponseBody String updateContentList(DataMap parameters) throws Exception {
		CustomUser user = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println("selectContentList ::: "+ parameters.toString() );
		Type listType = new TypeToken<List<Content>>() {}.getType();
		List<Content> contentList = new Gson().fromJson(DataTableUtil.getParamsAsJson(parameters,new String[]{"CONTENT_SEQ","edit"}), listType);
		int i = 0;
		List<Content> list = new ArrayList<Content>();
		if(contentList != null && contentList.size() > 0){
			for(Content content : contentList){
				content.setUSER_SEQ(user.getUserSeq());
				content.setUSER_ID(user.getUsername());
				i = contentService.updateContent(content);
				list.addAll(contentService.selectContentList(content));
			}
		}
		Map<String,Object> map = new HashMap<>();
		map.put("draw", "1");
		map.put("recordsTotal", list.size());
		map.put("recordsFiltered", list.size());
		map.put("data", list);
		
		String result = new Gson().toJson(map);
		
		return result;
	}
	@RequestMapping
	public @ResponseBody String deleteContentList(DataMap parameters) throws Exception {
		CustomUser user = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println("selectContentList ::: "+ parameters.toString() );
		Type listType = new TypeToken<List<Content>>() {}.getType();
		List<Content> contentList = new Gson().fromJson(DataTableUtil.getParamsAsJson(parameters,new String[]{"CONTENT_SEQ","remove"}), listType);
		int i = 0;
		List<Content> list = new ArrayList<Content>();
		if(contentList != null && contentList.size() > 0){
			for(Content content : contentList){
				content.setUSER_SEQ(user.getUserSeq());
				content.setUSER_ID(user.getUsername());
				i = contentService.deleteContent(content);
			}
		}
		Map<String,Object> map = new HashMap<>();
		String result = new Gson().toJson(map);
		
		return result;
	}
	
}
