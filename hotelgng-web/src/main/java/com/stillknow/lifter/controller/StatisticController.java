package com.stillknow.lifter.controller;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.stillknow.common.global.utils.DataMap;
import com.stillknow.common.security.domain.CustomUser;
import com.stillknow.lifter.model.Statistic;
import com.stillknow.lifter.service.StatisticService;
//import org.springframework.util.StringUtils;

@Controller
@RequestMapping(value="/pn/statistic/*.do")
public class StatisticController<K> {
	
	private static final Logger logger = LoggerFactory.getLogger(StatisticController.class);
	
	private ApplicationContext applicationContext;
	
	@Autowired
	private ServletContext servletContext;
	
	@Autowired
	private StatisticService statisticService;
	
	@RequestMapping
	public @ResponseBody String selectTempStatisticList(DataMap parameters) throws Exception {
		CustomUser user = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println("selectStatisticList ::: "+ parameters.toString() );
		System.out.println(parameters.toString());
		Statistic statistic = new Gson().fromJson((String) parameters.get("STATISTIC"), Statistic.class);
		System.out.println("selectStatisticList ::: statistic :: "+ statistic );		
		if(statistic==null) statistic=new Statistic();
		statistic.setUSER_SEQ(user.getUserSeq());
		statistic.setUSER_ID(user.getUsername());
		List<Statistic> list = statisticService.selectTempStatisticList(statistic);
		Map<String,Object> map = new HashMap<>();
		map.put("draw", "1");
		map.put("recordsTotal", list.size());
		map.put("recordsFiltered", list.size());
		map.put("data", list);
		
		String result = new Gson().toJson(map);	
		return result;
	}
	
}
