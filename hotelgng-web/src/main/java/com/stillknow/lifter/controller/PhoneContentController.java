package com.stillknow.lifter.controller;


import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stillknow.common.global.utils.DataMap;
import com.stillknow.common.security.domain.CustomUser;
import com.stillknow.lifter.model.Content;
import com.stillknow.lifter.model.Phone;
import com.stillknow.lifter.model.PhoneContent;
import com.stillknow.lifter.service.PhoneContentService;
//import org.springframework.util.StringUtils;

/**
 * @author	: Administrator
 * @date	: 2016. 5. 27. 오후 1:48:29
 * @project : lifter
 * @package : com.stillknow.lifter.controller
 * @file	: PhoneContentController.java
 */
@Controller
@RequestMapping(value="/pn/pc/*.do")
public class PhoneContentController {
	
	private static final Logger logger = LoggerFactory.getLogger(PhoneContentController.class);
	
	private ApplicationContext applicationContext;
	
	@Autowired
	private ServletContext servletContext;
	
	@Autowired
	private PhoneContentService phoneContentService;
	
	@RequestMapping
	public @ResponseBody String selectPhoneContentList(DataMap parameters) throws Exception {
		CustomUser user = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println("selectPhoneContentList ::: "+ parameters.toString() );
		PhoneContent phoneContent = new Gson().fromJson((String) parameters.get("PHONE_CONTENT"), PhoneContent.class);
		if(phoneContent==null) phoneContent=new PhoneContent();
		phoneContent.setUSER_SEQ(user.getUserSeq());
		phoneContent.setUSER_ID(user.getUsername());
		List<PhoneContent> list = phoneContentService.selectPhoneContentList(phoneContent);
		Map<String,Object> map = new HashMap<>();
		map.put("data", list);
		
		String result = new Gson().toJson(map);		
		return result;
	}
	@RequestMapping
	public @ResponseBody String selectPhoneList(DataMap parameters) throws Exception {
		CustomUser user = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println("selectPhoneContentList ::: "+ parameters.toString() );
		Phone phone = new Gson().fromJson((String) parameters.get("PHONE"), Phone.class);
		if(phone==null) phone=new Phone();
		phone.setUSER_SEQ(user.getUserSeq());
		phone.setUSER_ID(user.getUsername());
		List<Phone> list = phoneContentService.selectPhoneList(phone);
		Map<String,Object> map = new HashMap<>();
		map.put("data", list);
		String result = new Gson().toJson(map);		
		return result;
	}
	@RequestMapping
	public @ResponseBody String selectContentList(DataMap parameters) throws Exception {
		CustomUser user = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println("selectPhoneContentList ::: "+ parameters.toString() );
		Content content = new Gson().fromJson((String) parameters.get("CONTENT"), Content.class);
		if(content==null) content=new Content();
		content.setUSER_SEQ(user.getUserSeq());
		content.setUSER_ID(user.getUsername());
		List<Content> list = phoneContentService.selectContentList(content);
		Map<String,Object> map = new HashMap<>();
		map.put("data", list);
		String result = new Gson().toJson(map);		
		return result;
	}
	@RequestMapping
	public @ResponseBody String insertPhoneContentList(DataMap parameters) throws Exception {
		CustomUser user = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println("selectPhoneContentList ::: "+ parameters.toString() );
		Type listType = new TypeToken<List<PhoneContent>>() {}.getType();
		List<PhoneContent> phoneContentList = new Gson().fromJson((String) parameters.get("PHONE_CONTENT"), listType);
		int i = 0;
		if(phoneContentList != null && phoneContentList.size() > 0){
			for(PhoneContent phoneContent : phoneContentList){
				phoneContent.setUSER_SEQ(user.getUserSeq());
				phoneContent.setUSER_ID(user.getUsername());
				phoneContentService.insertPhoneContent(phoneContent);
				i=1;
			}
		}
		return i+"";
	}
	@RequestMapping
	public @ResponseBody String updatePhoneContentList(DataMap parameters) throws Exception {
		CustomUser user = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println("selectPhoneContentList ::: "+ parameters.toString() );
		Type listType = new TypeToken<List<PhoneContent>>() {}.getType();
		List<PhoneContent> phoneContentList = new Gson().fromJson((String) parameters.get("PHONE_CONTENT"), listType);
		int i = 0;
		if(phoneContentList != null && phoneContentList.size() > 0){
			for(PhoneContent phoneContent : phoneContentList){
				phoneContent.setUSER_SEQ(user.getUserSeq());
				phoneContent.setUSER_ID(user.getUsername());
				i = phoneContentService.updatePhoneContent(phoneContent);
			}
		}
				
		return i+"";
	}
	@RequestMapping
	public @ResponseBody String deletePhoneContentList(DataMap parameters) throws Exception {
		CustomUser user = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Type listType = new TypeToken<List<PhoneContent>>() {}.getType();
		List<PhoneContent> phoneContentList = new Gson().fromJson((String) parameters.get("PHONE_CONTENT"), listType);
		int i = 0;
		if(phoneContentList != null && phoneContentList.size() > 0){
			for(PhoneContent phoneContent : phoneContentList){
				phoneContent.setUSER_SEQ(user.getUserSeq());
				phoneContent.setUSER_ID(user.getUsername());
				i = phoneContentService.deletePhoneContent(phoneContent);
			}
		}
		return i+"";
	}
	
}
