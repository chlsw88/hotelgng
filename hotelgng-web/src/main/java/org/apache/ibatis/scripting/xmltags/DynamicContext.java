package org.apache.ibatis.scripting.xmltags;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.ognl.OgnlContext;
import org.apache.ibatis.ognl.OgnlException;
import org.apache.ibatis.ognl.OgnlRuntime;
import org.apache.ibatis.ognl.PropertyAccessor;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.session.Configuration;
 
public class DynamicContext {
	public static final String PARAMETER_OBJECT_KEY = "_parameter";
	  public static final String DATABASE_ID_KEY = "_databaseId";

	  static {
	    OgnlRuntime.setPropertyAccessor(ContextMap.class, new ContextAccessor());
	  }

	  private final ContextMap bindings;
	  private final StringBuilder sqlBuilder = new StringBuilder();
	  private int uniqueNumber = 0;

	  public DynamicContext(Configuration configuration, Object parameterObject) {
	    if (parameterObject != null && !(parameterObject instanceof Map)) {
	      MetaObject metaObject = configuration.newMetaObject(parameterObject);
	      bindings = new ContextMap(metaObject);
	    } else {
	      bindings = new ContextMap(null);
	    }
	    bindings.put(PARAMETER_OBJECT_KEY, parameterObject);
	    bindings.put(DATABASE_ID_KEY, configuration.getDatabaseId());
	  }

	  public Map<String, Object> getBindings() {
	    return bindings;
	  }

	  public void bind(String name, Object value) {
	    bindings.put(name, value);
	  }

	  public void appendSql(String sql) {
	    sqlBuilder.append(sql);
	    sqlBuilder.append(" ");
	  }

	  public String getSql() {
	    return sqlBuilder.toString().trim();
	  }

	  public int getUniqueNumber() {
	    return uniqueNumber++;
	  }
    
    static
    {
        OgnlRuntime.setPropertyAccessor(ContextMap.class, new ContextAccessor());
    }
    
    static class ContextAccessor implements PropertyAccessor {

        public Object getProperty(Map context, Object target, Object name)
            throws OgnlException {
          Map map = (Map) target;

          Object result = map.get(name);
          if (result != null) {
            return result;
          }

          Object parameterObject = map.get(PARAMETER_OBJECT_KEY);
          if (parameterObject instanceof Map) {
        	  return ((Map)parameterObject).get(name);
          }

          return null;
        }

        public void setProperty(Map context, Object target, Object name, Object value)
            throws OgnlException {
          Map map = (Map) target;
          map.put(name, value);
        }
      }
	public static class ContextMap extends HashMap<String, Object>
    {
        private static final long serialVersionUID = 2977601501966151582L;
        
        private MetaObject parameterMetaObject;
        
        public ContextMap(MetaObject parameterMetaObject)
        {
            this.parameterMetaObject = parameterMetaObject;
        }
        
        @Override
        public Object put(String key, Object value) {
          return super.put(key, value);
        }
        
        @Override
        public Object get(Object key) {
          String strKey = (String) key;
          if (super.containsKey(strKey)) {
            return super.get(strKey);
          }

          if (parameterMetaObject != null) {
            Object object = parameterMetaObject.getValue(strKey);
            // issue #61 do not modify the context when reading
//            if (object != null) { 
//              super.put(strKey, object);
//            }

            return object;
          }

          return null;
        }
        
        // //////////////////////////////////////////////////////////////
        //
        // Custom Function 추가
        //
        // //////////////////////////////////////////////////////////////
        /**
         * <pre>
         * object 값이 존재하는지 여부를 체크하기 위한 함수
         * 
         * <code>&lt;if test="empty(value)"&gt;</code>
         * </pre>
         * 
         * @param obj
         * @return
         */
        public boolean isEmpty(Object obj)
        {
            if (obj == null) return true;
            
            if (obj instanceof String)
            {
                return ((String) obj).equals("");
            }
            else if (obj instanceof List)
            {
                return ((List<?>) obj).isEmpty();
            }
            else if (obj instanceof Map)
            {
                return ((Map<?, ?>) obj).isEmpty();
            }
            else if (obj instanceof Object[])
            {
                return Array.getLength(obj) == 0;
            }
            else
            {
                return obj == null;
            }
        }
        
        public boolean isNotEmpty(Object obj){
        	return !isEmpty(obj);
        }
        
        /**
         * <pre>
         * 문자열에 검색 문자가 존재하는지 체크하기 위한 함수
         * </pre>
         * 
         * @param text
         *            대상 문자열
         * @param search
         *            찾을 문자열
         * @return
         */
        public boolean contains(String text, String search)
        {
            return text.contains(search);
        }
        
        /**
         * <pre>
         * 오브젝트의 길이나 개수를 반환하는 함수
         * 
         * <code>&lt;if test="length(value)"&gt;</code>
         * </pre>
         * 
         * @param obj
         * @return
         */
        public int length(Object obj)
        {
            if (obj == null) return 0;
            
            if (obj instanceof String)
            {
                return ((String) obj).length();
            }
            else if (obj instanceof List)
            {
                return ((List<?>) obj).size();
            }
            else if (obj instanceof Map)
            {
                return ((Map<?, ?>) obj).size();
            }
            else if (obj instanceof Object[])
            {
                return Array.getLength(obj);
            }
            else
            {
                return 0;
            }
        }
    }
}